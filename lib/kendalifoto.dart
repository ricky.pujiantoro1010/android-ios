// ignore_for_file: depend_on_referenced_packages, import_of_legacy_library_into_null_safe, prefer_const_constructors_in_immutables, library_private_types_in_public_api, prefer_const_constructors, use_build_context_synchronously, avoid_unnecessary_containers, prefer_const_literals_to_create_immutables, avoid_function_literals_in_foreach_calls, avoid_print

import 'dart:io';
import 'dart:core';
import 'dart:convert';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:kendali_batu/login.dart';
import 'package:kendali_batu/model/model_skpd.dart';
import 'package:kendali_batu/model/model_paket.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:search_choices/search_choices.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:version_check/version_check.dart';
import 'catatankendali.dart';
// import 'login.dart';
import 'package:path/path.dart';
import 'package:geolocator/geolocator.dart' as geo;

//////////////////////////////// Class Kendali /////////////////////////////////////////////////////
//void main() => runApp(Kendali());

class KendaliFoto extends StatefulWidget {
  final String nama;
  final String tipe;

  KendaliFoto(this.nama, this.tipe, {Key? key}) : super(key: key);
  @override
  _KendaliFotoState createState() => _KendaliFotoState();
}

class _KendaliFotoState extends State<KendaliFoto> {
  File? _imagefix;
  final GlobalKey<FormState> _key = GlobalKey<FormState>();

//////////////////////////////////////// inisialisasi value //////////////////////////

  String skpd = '';
  String? minggu;
  String paket = '';
  String idUserPengirim = '1';
  String idup = '';
  Satker? _satker;
  Paket? _pakets;
  DateTime waktuSekarang = DateTime.now();
  DateTime? waktuMulai;
  String? tempDate1;
  String? tempDate2;
  String? countRencana;
  geo.Position? posisiFoto;
  double? lat1;
  double? long1;
  String? address;
  String versionApp = "";
  String packagename = "";
  String versiLokal = "";
  Widget? childPkt;
  Widget? childPd;
  String? dropdown;
  String jam = '';
  String tanggal = '';
  final lat = TextEditingController();
  final long = TextEditingController();
  int? countDate;

//////////////////////////////////////// server URL ///////////////////////////////////////

  // final urlPD = "http://ekenda.sidoarjokab.go.id/settingandroid/getskpd.php";
  // final urlP = "http://ekenda.sidoarjokab.go.id/settingandroid/getprogram.php";
  // final urlK = "http://ekenda.sidoarjokab.go.id/settingandroid/getkegiatan.php";
  // final urlPK = "http://ekenda.sidoarjokab.go.id/settingandroid/getpaket.php";
  // final urlCountMinggu =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/getweek.php";
  // final urlTgl =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/gettglmulai.php";

  // local batu server

  final urlPD = "http://ekenda.batukota.go.id/settingandroidbatu/getskpd.php";
  // final urlP = "http://ekenda.batukota.go.id/settingandroidbatu/getprogram.php";
  // final urlK =
  //     "http://ekenda.batukota.go.id/settingandroidbatu/getkegiatan.php";
  final urlPK = "http://ekenda.batukota.go.id/settingandroidbatu/getpaket.php";
  final urlCountMinggu =
      "http://ekenda.batukota.go.id/settingandroidbatu/getweek.php";
  final urlTgl =
      "http://ekenda.batukota.go.id/settingandroidbatu/gettglmulai.php";

  ////////////////////////////////////// definisi data map //////////////////////////////

  // ignore: deprecated_member_use
  List dataPD = [];
  // ignore: deprecated_member_use
  List dataPKt = [];
  // ignore: deprecated_member_use
  List dataW = [];

  List dataTgl = [];

  List dataMingguAkhir = [];

  List<DropdownMenuItem> items1 = [];
  List<DropdownMenuItem> items2 = [];
  List dataVersion = [];

  // cek version
  cekVersionLokal() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      versiLokal = packageInfo.version;
    });
    print(packageInfo.version);
    advancedStatusCheck();
  }

  advancedStatusCheck() async {
    final versionCheck = VersionCheck(
      // iOSId: 'com.google.Vespa',
      packageVersion: versiLokal,
      packageName: 'ekenda.sda',
      showUpdateDialog: customShowUpdateDialog,
    );
    await versionCheck.checkVersion(this.context);
  }

  void customShowUpdateDialog(BuildContext context, VersionCheck versionCheck) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: Text('Pembaruan aplikasi telah tersedia !'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                  'Segera perbarui aplikasi dari versi ${versionCheck.packageVersion}'),
              Text('ke versi ${versionCheck.storeVersion}'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Perbarui'),
            onPressed: () async {
              await versionCheck.launchStore();
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text('Tutup'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  /////////////////////////////////////// get data PD //////////////////////////////////////

  Future<String> getPD(BuildContext context) async {
    try {
      EasyLoading.show();
      var res = await http.get(Uri.parse(urlPD));
      EasyLoading.dismiss();

      setState(() {
        dataPD = satkerFromJson(res.body);
      });
      if (dataPD.isEmpty) {
        //EasyLoading.dismiss();
        childPd = Container(
          child: SearchChoices.single(
            displayClearIcon: false,
            underline: Padding(
              padding: EdgeInsets.all(5),
            ),
            items: items1,
            value: paket,
            hint: Row(
              children: [
                Text('Pilih SKPD'),
                Text(
                  ' *',
                  style: TextStyle(color: Colors.red),
                ),
              ],
            ),
            searchHint: Text("Cari Paket"),
            onChanged: (valuePkt) {
              setState(() {
                //minggu = ;
              });
              //getCatatan(paket);
            },
            dialogBox: true,
            isExpanded: true,
          ),
        );
        EasyLoading.showError('Server Error\nMohon coba beberapa saat lagi');
      } else {
        dataPD.forEach((pd) {
          items1.add(DropdownMenuItem(
            value: pd,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                pd.namapd,
              ),
            ),
          ));
        });
        childPd = SearchChoices.single(
          underline: Padding(
            padding: EdgeInsets.all(5),
          ),
          displayClearIcon: false,
          items: items1,
          value: skpd,
          hint: Row(
            children: [
              Text('Pilih SKPD'),
              Text(
                ' *',
                style: TextStyle(color: Colors.red),
              ),
            ],
          ),
          searchHint: Text("Cari SKPD"),
          onChanged: (valuePd) {
            setState(() {
              _satker = valuePd;
              skpd = _satker!.idpd;
              namapkt = null;
              minggu = null;
              catatan = null;
              items2.clear();
              childPkt = Container(
                child: SearchChoices.single(
                  displayClearIcon: false,
                  underline: Padding(
                    padding: EdgeInsets.all(5),
                  ),
                  items: items2,
                  value: paket,
                  hint: Row(
                    children: [
                      Text('Pilih Paket'),
                      Text(
                        ' *',
                        style: TextStyle(color: Colors.red),
                      ),
                    ],
                  ),
                  searchHint: Text("Cari Paket"),
                  onChanged: (valuePkt) {
                    setState(() {
                      //minggu = ;
                    });
                    //getCatatan(paket);
                  },
                  dialogBox: true,
                  isExpanded: true,
                ),
              );
            });
            getPaket(skpd, context);
          },
          dialogBox: true,
          isExpanded: true,
        );
      }
    } catch (e) {
      EasyLoading.showError('Koneksi terputus\n$e');
    }

    return "success";
  }

  /////////////////////////////////////// get data Paket //////////////////////////////////////

  // ignore: missing_return
  Future<String> getPaket(idpd, BuildContext context) async {
    EasyLoading.show();
    final res = await http.post(Uri.parse(urlPK), body: {'idpd': idpd});
    EasyLoading.dismiss();
    setState(() {
      dataPKt = paketFromJson(res.body);
    });
    if (dataPKt.isEmpty) {
      setState(() {
        paket = '';
        _pakets = null;
        namapkt = null;

        childPkt = Container(
          child: SearchChoices.single(
            displayClearIcon: false,
            underline: Padding(
              padding: EdgeInsets.all(5),
            ),
            items: items2,
            value: paket,
            hint: Row(
              children: [
                Text('Pilih Paket'),
                Text(
                  ' *',
                  style: TextStyle(color: Colors.red),
                ),
              ],
            ),
            searchHint: Text("Cari Paket"),
            onChanged: (valuePkt) {
              setState(() {});
            },
            dialogBox: true,
            isExpanded: true,
          ),
        );
      });
      EasyLoading.showInfo(
        'Paket tidak tersedia',
        duration: Duration(seconds: 3),
      );
    } else {
      setState(() {
        // ignore: unrelated_type_equality_checks

        dataPKt.forEach((pkt) {
          items2.add(DropdownMenuItem(
            value: pkt,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(pkt.pktNamaPaket),
            ),
          ));
        });

        childPkt = SearchChoices.single(
          underline: Padding(
            padding: EdgeInsets.all(5),
          ),
          displayClearIcon: false,
          items: items2,
          value: paket,
          hint: Row(
            children: [
              Text('Pilih Paket'),
              Text(
                ' *',
                style: TextStyle(color: Colors.red),
              ),
            ],
          ),
          searchHint: Text("Cari Paket"),
          onChanged: (valuePkt) {
            setState(() {
              _pakets = valuePkt;
              paket = _pakets!.pktId;
              namapkt = null;
              minggu = null;
              catatan = null;
            });
            getTgl(paket, context);
            getMingguAkhir(paket, context);
            getCatatan(paket);
          },
          dialogBox: true,
          isExpanded: true,
        );
      });
    }
    return '';
  }

  Future<String> getTgl(idRup, BuildContext context) async {
    final res = await http.post(Uri.parse(urlTgl), body: {'id_rup': idRup});
    final resBody = json.decode(res.body);

    setState(() {
      dataTgl = resBody;
    });

    //print(resBody);
    if (resBody.length == 0) {
      EasyLoading.showInfo(
        'Target belum di isi !',
        duration: Duration(seconds: 3),
      );
    }
    return "success";
  }

  // get minggu akhir
  Future<String> getMingguAkhir(kodeRup, BuildContext context) async {
    final res =
        await http.post(Uri.parse(urlCountMinggu), body: {'idPkt': kodeRup});
    final resBody = json.decode(res.body);

    setState(() {
      dataMingguAkhir = resBody;
    });

    if (resBody.length == 0) {
      EasyLoading.showInfo(
        'Target belum di isi !',
        duration: Duration(seconds: 3),
      );
    }

    return "success";
  }
///////////////////////////////// List khusus catatan ///////////////////////////////////////

  final urlcatatan =
      "http://ekenda.batukota.go.id/settingandroidbatu/getcatatan.php";
  // final urlcatatan =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/getcatatan.php";

  // ignore: deprecated_member_use
  List dataCatatan = [];
  String? catatan;
  String? namapkt;

  Future<String> getCatatan(idpkt) async {
    var res = await http.post(Uri.parse(urlcatatan), body: {'pkt_id': idpkt});
    var resBody = json.decode(res.body);

    setState(() {
      dataCatatan = resBody;
      catatan = dataCatatan.map((e) => {e['catatan']}).toString();
      namapkt = dataCatatan.map((e) => {e['namapkt']}).toString();
    });

    return "success";
  }

  PickedFile? pickedFile;
  Future getImage() async {
    try {
      final picker = ImagePicker();
      // ignore: deprecated_member_use
      pickedFile = await picker.getImage(
        // preferredCameraDevice: CameraDevice.front,
        source: ImageSource.gallery,
      );

      setState(() {
        EasyLoading.show(status: 'loading ...');
        _imagefix = File(pickedFile!.path);
        // set opd
        skpd = dataTgl.map((e) => {e['id_satker']}).toString().replaceAll(
            RegExp(
              "[({})]",
            ),
            "");
        ////////////// set jumlah minggu akhir /////////////
        countRencana = dataMingguAkhir
            .map((e) => {e['mingguterakhir']})
            .toString()
            .replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");

        //////////// set waktu mulai perencanaan ///////////
        tempDate1 = dataTgl.map((e) => {e['dateStart']}).toString();
        tempDate2 = dataTgl.map((e) => {e['dateEnd']}).toString();
        waktuMulai = DateTime.parse(
          tempDate1!.replaceAll(
              RegExp(
                "[({})]",
              ),
              ""),
        );
        /////////// tanggal /////////////////

        final DateFormat formatter = DateFormat('dd-MM-yyyy');
        tanggal = formatter.format(waktuSekarang);

        ///////////// jam /////////////////

        jam = DateFormat('kk:mm:ss').format(waktuSekarang);
        print(jam);

        ///////////////// get hari sekarang //////////////////////

        final day1 = DateFormat('EEEE').format(waktuSekarang);
        final day2 = DateFormat('EEEE').format(waktuMulai!);

        ///////////////////// hitung jarak hari (untuk menentukan minggu) //////////////

        final difference = waktuSekarang.difference(waktuMulai!).inDays;
        final hasil = difference / 7;
        final hasil1 = (hasil).toDouble();
        final finalHasil = hasil1.ceil();

        ///////////////////// print hasil //////////////////
        print(difference);
        print(waktuMulai);
        print(finalHasil);
        print(day1);
        print(day2);

        ////////////////////// cek kondisi tanggal terkini & set minggu //////////////////

        if (finalHasil == 0) {
          countDate = 1;
        } else if (finalHasil > int.parse(countRencana!)) {
          countDate = int.parse(
            countRencana!.replaceAll(
                RegExp(
                  "[({})]",
                ),
                ""),
          );
        } else {
          // if (day1 == day2) {
          //   countDate = finalHasil + 1;
          // } else {
          countDate = finalHasil;
          // }
        }
      });
      EasyLoading.dismiss();
    } catch (e) {
      EasyLoading.showError('Anda belum mengambil foto\n$e');
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////

  logout(BuildContext context) {
    // set up the buttons
    // ignore: deprecated_member_use
    Widget cancelButton = TextButton(
      child: Text("Tidak"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // ignore: deprecated_member_use
    Widget continueButton = TextButton(
      child: Text("Ya"),
      onPressed: () {
        Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(pageBuilder: (BuildContext context,
                Animation animation, Animation secondaryAnimation) {
              return LoginAdmin(
                title: 'W',
              );
            }, transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              return SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(1.0, 0.0),
                  end: Offset.zero,
                ).animate(animation),
                child: child,
              );
            }),
            (Route route) => false);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Logout"),
      content: Text("Apakah anda yakin ingin logout?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  void initState() {
    super.initState();
    // _checkVersion(this.context);
    // this.cekVersionLokal();

    childPkt = Container(
      child: SearchChoices.single(
        displayClearIcon: false,
        underline: Padding(
          padding: EdgeInsets.all(5),
        ),
        items: items2,
        value: _pakets,
        hint: Row(
          children: [
            Text('Pilih Paket'),
            Text(
              ' *',
              style: TextStyle(color: Colors.red),
            ),
          ],
        ),
        searchHint: Text("Cari Paket"),
        onChanged: (valuePkt) {
          setState(() {
            //minggu = ;
          });
          //getCatatan(paket);
        },
        dialogBox: true,
        isExpanded: true,
      ),
    );
    getPD(this.context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                //duration: Duration(seconds: 1),
                margin: EdgeInsets.only(bottom: 30),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: ExactAssetImage('lib/aset/logobayang.png'),
                      fit: BoxFit.cover),
                ),
                child: Container(
                  width: 500,
                  decoration: BoxDecoration(
                      //backgroundBlendMode: BlendMode.lighten,
                      borderRadius: BorderRadius.circular(70),
                      color: Colors.grey[800]!.withOpacity(0.3)),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0, bottom: 10),
                        child: Image.asset(
                          'lib/aset/userlogo.png',
                          scale: 10,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(widget.nama,
                            style:
                                TextStyle(fontFamily: 'Paytone', fontSize: 15)),
                      ),
                    ],
                  ),
                ),
              ),
              // ListTile(
              //   leading: Image.asset(
              //     'lib/aset/dashboard.png',
              //     scale: 14,
              //   ),
              //   title: Text(
              //     'Dashboard',
              //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              //   ),
              //   onTap: () {
              //     // Navigator.push(
              //     //   context,
              //     //   MaterialPageRoute(
              //     //     builder: (BuildContext context) =>
              //     //         Dashboard(widget.nama, widget.tipe),
              //     //   ),
              //     // );
              //   },
              // ),
              ListTile(
                leading: Image.asset(
                  'lib/aset/picture.png',
                  scale: 15,
                ),
                title: Text(
                  'Upload foto paket',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          KendaliFoto(widget.nama, widget.tipe),
                    ),
                  );
                },
              ),
              ListTile(
                leading: Image.asset(
                  'lib/aset/logout.png',
                  scale: 20,
                ),
                title: Text(
                  'Logout',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                onTap: () {
                  // set up the buttons
                  // ignore: deprecated_member_use
                  Widget cancelButton = TextButton(
                    child: Text("Tidak"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                  // ignore: deprecated_member_use
                  Widget continueButton = TextButton(
                    child: Text("Ya"),
                    onPressed: () async {
                      SharedPreferences preferences =
                          await SharedPreferences.getInstance();
                      preferences.clear();
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(pageBuilder: (BuildContext context,
                              Animation animation,
                              Animation secondaryAnimation) {
                            return LoginAdmin(
                              title: '',
                            );
                          }, transitionsBuilder: (BuildContext context,
                              Animation<double> animation,
                              Animation<double> secondaryAnimation,
                              Widget child) {
                            return SlideTransition(
                              position: Tween<Offset>(
                                begin: const Offset(1.0, 0.0),
                                end: Offset.zero,
                              ).animate(animation),
                              child: child,
                            );
                          }),
                          (Route route) => false);
                    },
                  );

                  // set up the AlertDialog
                  AlertDialog alert = AlertDialog(
                    title: Text("Logout"),
                    content: Text("Apakah anda yakin ingin logout?"),
                    actions: [
                      cancelButton,
                      continueButton,
                    ],
                  );

                  // show the dialog
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return alert;
                    },
                  );
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Padding(
            padding: const EdgeInsets.only(left: 78.0),
            child: Row(
              children: [
                Text(
                  'E',
                  style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 20),
                ),
                Text(
                  ' - KENDA',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            margin: EdgeInsets.all(20),
            width: 700,
            child: Form(
              key: _key,
              //autovalidate: true,
              child: ListView(
                children: <Widget>[
                  //////////////////////////////// Pilih PD ///////////////////////////////////////////////////////////////////////////////

                  Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40.0),
                        border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 2),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 15, left: 15),
                        child: childPd,
                      ),
                    ),
                  ),
                  //Text("Kamu memilih PD $skpd"),

                  //////////////////////////////////// Pilih Paket //////////////////////////////////////////////////////////

                  Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40.0),
                        border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 2),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 15, left: 15),
                        child: childPkt,
                      ),
                    ),
                  ),
                  // Spacer(),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      'Kode SKPD : $skpd',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      'Kode RUP : $paket',
                      textAlign: TextAlign.center,
                    ),
                  ),

                  // catatan
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Row(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Container(
                              // width: MediaQuery.of(context).size.width / 3,
                              // ignore: deprecated_member_use
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Colors.green[400],
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.photo,
                                    size: 35,
                                  ),
                                ),
                                onPressed: () async {
                                  getImage();
                                },
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Container(
                              // width: MediaQuery.of(context).size.width / 3,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      Colors.green[400],
                                    ),
                                  ),
                                  child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        Icons.note_add,
                                        size: 35,
                                      )),
                                  onPressed: () {
                                    // ignore: unnecessary_null_comparison
                                    if (skpd == null && paket == null) {
                                      final snackBar = SnackBar(
                                        content: Row(
                                          children: [
                                            const Text(
                                                'Harap pilih SKPD & Paket terlebih dahulu ! '),
                                            const Text(
                                              '*',
                                              style:
                                                  TextStyle(color: Colors.red),
                                            ),
                                          ],
                                        ),
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    } else {
                                      var route = MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            CatatanKendali(
                                                paket, catatan!, namapkt!),
                                      );
                                      Navigator.of(context).push(route);
                                    }
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Center(
                      child: _imagefix == null
                          ? Container(
                              width: 350,
                              height: 200,
                              color: Colors.grey[200],
                              child: SizedBox(
                                child: Center(
                                  child: Text('Belum ada gambar !'),
                                ),
                              ),
                            )
                          : Stack(
                              children: <Widget>[
                                Image.file(_imagefix!),
                                // Column(
                                //   // mainAxisSize: MainAxisSize.max,
                                //   mainAxisAlignment: MainAxisAlignment.center,
                                //   children: [
                                //     Padding(
                                //       padding:
                                //           const EdgeInsets.only(bottom: 3.0),
                                //       child: Text(
                                //         'Pukul: $jam',
                                //         style: TextStyle(
                                //             backgroundColor: Colors.white70,
                                //             fontSize: 15),
                                //         // textAlign: TextAlign.center,
                                //       ),
                                //     ),
                                //     Padding(
                                //       padding:
                                //           const EdgeInsets.only(bottom: 3.0),
                                //       child: Text(
                                //         "Tanggal : $tanggal",
                                //         style: TextStyle(
                                //             backgroundColor: Colors.white70,
                                //             fontSize: 15),
                                //         // textAlign: TextAlign.center,
                                //       ),
                                //     ),
                                //   ],
                                // ),
                              ],
                            ),
                    ),
                  ),
                  //////////// tombol upload ///////////////

                  Container(
                    height: 65,
                    padding: EdgeInsets.only(
                        top: 10, left: 10, right: 10, bottom: 0.20),
                    // ignore: deprecated_member_use
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          Colors.green[400],
                        ),
                      ),
                      // textColor: Colors.white,
                      // color: Colors.green,
                      child: Text(
                        'Upload foto',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      onPressed: () {
                        upload(context);
                      },
                    ),
                  ),
                ],
              ),
              //key: _formkey,
            ),
          ),
        ),
      ),
    );
  }

  ///////////////////////////////// function upload ///////////////////////////////////////////////////////////////

  upload(BuildContext context) async {
    try {
      ////////////////////////////////////  url upload dan update perencanaan /////////////////////////////////////////////////

      var url = Uri.parse(
          // "http://10.0.2.2/settingandroidbatu/uploadimagebyadmin.php");
          "http://ekenda.batukota.go.id/settingandroidbatu/uploadimagebyadmin.php");
      // var url = Uri.parse("http://10.0.2.2/settingandroid/uploadimg.php");
      // var urlIdup = Uri.parse(
      //     "http://rds.sidoarjokab.go.id/settingandroid/updateidup.php");

      ////////////////////////////////// Method Kirim data /////////////////////////////////////////////////////////////////////////

      var request = http.MultipartRequest("POST", url);
      // var request2 = new http.MultipartRequest("POST", urlIdup);

      ////////////////////////////////// definisi fields yg akan di inputkan ////////////////////////////////////////////////////////////

      request.fields['idpkt'] = paket;
      request.fields['idrenc'] = paket + countDate.toString();
      request.fields['ukid'] = skpd;
      request.fields['jam'] = jam;
      request.fields['tgl'] = tanggal;
      request.fields['minggu'] = countDate.toString();

      ////////////////////// kirim image definisi /////////////////////////////////////////////////////////////////////////////////
      var stream = http.ByteStream(DelegatingStream(_imagefix!.openRead()));
      var length = await _imagefix!.length();
      var multipartFile = http.MultipartFile("image", stream, length,
          filename: basename(_imagefix!.path));
      request.files.add(multipartFile);
      EasyLoading.show(status: 'loading ...');
      ///////////////////////// kirim response ///////////////////////////////

      var response = await request.send();

      //////////////////////// warning untuk kirim data //////////////////////

      if (response.statusCode == 200) {
        EasyLoading.showSuccess(
          'Upload Success',
          duration: Duration(seconds: 3),
        );
        setState(() {
          imageCache.clear();
          _imagefix = null;
        });
      } else if (response.statusCode == 403 || response.statusCode == 400) {
        EasyLoading.showError(
          'Gagal Upload/Bad response',
          duration: Duration(seconds: 3),
        );
      } else if (response.statusCode == 500) {
        EasyLoading.showError(
          'Server sedang perbaikan',
          duration: Duration(seconds: 5),
        );
      }
      EasyLoading.dismiss();
    } catch (e) {
      debugPrint("error $e");
      EasyLoading.showInfo('Isi Semua Field\n$e');
    }
  }
}
