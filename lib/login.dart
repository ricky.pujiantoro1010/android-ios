// ignore_for_file: unnecessary_new, import_of_legacy_library_into_null_safe, use_build_context_synchronously, avoid_unnecessary_containers

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:kendali_batu/animasigeserkiri.dart';
import 'package:kendali_batu/fotopengawas.dart';
import 'package:kendali_batu/kendalifoto.dart';
import 'package:kendali_batu/registerpengawas.dart';
import 'package:kendali_batu/scanqrcode.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginAdmin extends StatefulWidget {
  const LoginAdmin({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<LoginAdmin> createState() => _LoginAdminState();
}

class _LoginAdminState extends State<LoginAdmin> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final username = TextEditingController();
  final password = TextEditingController();

  String nama = '';
  String kowil = '';
  String idPeng = '';
  String tipe = '';
  String usernametampung = '';
  String usernameValue = '';
  bool _isHidden = true;
  String cekPaket = '';
  String? cekNik;
  String versiLokal = '';
  String versiToko = '';

  List? dataNik;

// url batu
  final urlLogin = "http://ekenda.batukota.go.id/settingandroidbatu/login.php";
  final urlCekLogin =
      "http://ekenda.batukota.go.id/settingandroidbatu/cek_username.php";

  // toggle password showing
  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  // cek username

  void cekUsername() async {
    EasyLoading.show();
    var res =
        await http.post(Uri.parse(urlCekLogin), body: {'nik': username.text});

    //final resBody = json.decode(res.body);
    setState(() {
      dataNik = json.decode(res.body);
      cekNik = dataNik!.map((e) => {e['status']}).toString().replaceAll(
          new RegExp(
            "[({})]",
          ),
          "");
    });
    EasyLoading.dismiss();

    if (cekNik == '0') {
      EasyLoading.showError('Data tidak di temukan',
          duration: const Duration(seconds: 3));
    } else {
      _login();
    }
  }

  Future<String> _login() async {
    try {
      EasyLoading.show();
      List dataUser;

      final response = await http.post(Uri.parse(urlLogin), body: {
        "username": username.text,
        "password": password.text,
      });

      dataUser = json.decode(response.body);
      //print(datauser);
      if (username.text == "" || password.text == "") {
        EasyLoading.showError('Isi field terlebih dahulu!',
            duration: const Duration(seconds: 3));
      } else if (response.body == "[]") {
        EasyLoading.showError('Username tidak ditemukan',
            duration: const Duration(seconds: 3));
      } else if (dataUser[0]['msg'] == 0) {
        EasyLoading.showError('Password Anda Salah',
            duration: const Duration(seconds: 3));
      }

      setState(() {
        //dataUser = datauser;
        if (dataUser[0]['ac_tipe'] == "6") {
          nama = dataUser.map((e) => {e['nama_lengkap']}).toString();
          kowil = dataUser.map((e) => {e['id_pengawas']}).toString();
          tipe = dataUser.map((e) => {e['ac_tipe']}).toString().replaceAll(
              new RegExp(
                "[({})]",
              ),
              "");
        } else if (dataUser[0]['ac_tipe'] == "5" ||
            dataUser[0]['ac_tipe'] == "20") {
          nama = "Admin Pembangunan";
          tipe = dataUser.map((e) => {e['ac_tipe']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
        }
      });

      if (dataUser[0]['ac_tipe'] == "6") {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setString('username', username.text);
        preferences.setString('kowil', kowil);
        preferences.setString('nama', nama);
        preferences.setString('tipe', tipe);
        usernametampung = preferences.getString('username')!;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => FotoLapangan(
              kowil.replaceAll(
                  new RegExp(
                    "[({})]",
                  ),
                  ""),
              nama.replaceAll(
                  new RegExp(
                    "[({})]",
                  ),
                  ""),
              tipe.replaceAll(
                  new RegExp(
                    "[({})]",
                  ),
                  ""),
            ),
          ),
        );
      } else if (dataUser[0]['ac_tipe'] == "5" ||
          dataUser[0]['ac_tipe'] == "20") {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setString('username', username.text);
        preferences.setString('kowil', kowil);
        preferences.setString('nama', nama);
        preferences.setString('tipe', tipe);
        usernametampung = preferences.getString('username')!;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => KendaliFoto(
              nama.replaceAll(
                  RegExp(
                    "[({})]",
                  ),
                  ""),
              tipe.replaceAll(
                  RegExp(
                    "[({})]",
                  ),
                  ""),
            ),
          ),
        );
      }

      EasyLoading.dismiss();
    } catch (e) {
      EasyLoading.showError(
        'Username tidak ditemukan',
        duration: const Duration(seconds: 3),
      );
    }
    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: AutofillGroup(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(0),
              child: Container(
                decoration: const BoxDecoration(
                    // gradient: LinearGradient(
                    //   begin: Alignment.topRight,
                    //   end: Alignment.bottomLeft,
                    //   colors: [Colors.white, Colors.green[300], Colors.green],
                    // ),
                    color: Colors.green),
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Image.asset(
                        'lib/aset/Kota_Batu.png',
                        height: 110,
                      ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                    //   child: Image.asset(
                    //     'lib/aset/logo.png',
                    //     height: 110,
                    //   ),
                    // ),
                    Container(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: const Center(
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: Text(
                            '© Pemerintah Kota Batu\nBagian Administrasi Pembangunan',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                          // child: Text(
                          //   '© Pemerintah Kabupaten Sidoarjo\nBagian Administrasi Pembangunan',
                          //   style: TextStyle(
                          //       fontWeight: FontWeight.bold,
                          //       color: Colors.white),
                          //   textAlign: TextAlign.center,
                          // ),
                        ),
                      ),
                    ),

                    // Center(
                    //     child: Padding(
                    //   padding: const EdgeInsets.all(5.0),
                    //   child: Text('v' + versiToko),
                    // )),
                    // Padding(
                    //   padding: const EdgeInsets.only(bottom: 10),
                    //   child: Center(
                    //     child: Text(
                    //       'PEMERINTAH KOTA BATU',
                    //       style: TextStyle(
                    //           color: Colors.black,
                    //           fontWeight: FontWeight.w900,
                    //           fontSize: 25),
                    //     ),
                    //   ),
                    // ),
                    // Center(
                    //   child: Padding(
                    //     padding: const EdgeInsets.only(bottom: 15, top: 10),
                    //     child: Text(
                    //       'Halaman Login',
                    //       style: TextStyle(
                    //           fontSize: 17, fontWeight: FontWeight.bold),
                    //     ),
                    //   ),
                    // ),
                    Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50),
                        ),
                      ),
                      child: Column(
                        children: [
                          Form(
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 30),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    Text(
                                      'E',
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontWeight: FontWeight.w900,
                                          fontStyle: FontStyle.italic,
                                          fontSize: 30),
                                    ),
                                    Text(
                                      ' - KENDA',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w900,
                                          fontSize: 30),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                right: 30, left: 30, top: 30),
                            child: TextField(
                              keyboardType: TextInputType.text,
                              // autofocus: true,
                              enableSuggestions: false,
                              controller: username,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                prefixIcon: Icon(Icons.supervised_user_circle),
                                labelText: 'Username',
                              ),
                            ),
                            // child: InputHistoryTextField(
                            //   limit: 5,
                            //   enableHistory: true,
                            //   enableSave: true,
                            //   enableOpacityGradient: true,
                            //   historyKey: "01",
                            //   listStyle: ListStyle.List,
                            //   textEditingController: username,
                            //   decoration: InputDecoration(
                            //     border: OutlineInputBorder(),
                            //     prefixIcon:
                            //         const Icon(Icons.supervised_user_circle),
                            //     labelText: 'Username',
                            //   ),
                            // ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(30, 10, 30, 20),
                            child: TextField(
                              obscureText: _isHidden,
                              controller: password,
                              // enableSuggestions: true,
                              decoration: InputDecoration(
                                suffixIcon: InkWell(
                                  onTap: _togglePasswordView,
                                  child: _isHidden == true
                                      ? const Icon(Icons.visibility_off_rounded)
                                      : const Icon(Icons.visibility_rounded),
                                ),
                                prefixIcon: const Icon(Icons.key),
                                border: const OutlineInputBorder(),
                                labelText: 'Password',
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                // ignore: deprecated_member_use
                                child: TextButton(
                                  style: ButtonStyle(
                                    // textStyle:,
                                    backgroundColor: MaterialStateProperty.all(
                                      Colors.green,
                                    ),
                                  ),
                                  // TextButton.styleFrom(foregroundColor: Colors.white),
                                  // textColor: Colors.white,
                                  // color: Colors.green[400],
                                  child: const Padding(
                                    padding: EdgeInsets.all(5.0),
                                    child: Text(
                                      'Masuk',
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  onPressed: () {
                                    // cekUsername();
                                    _login();
                                  },
                                ),
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.only(bottom: 20, top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text(
                                      'Pengawas belum terdaftar ? ',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    GestureDetector(
                                      child: const Text(
                                        ' Daftar',
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 15,
                                            decoration:
                                                TextDecoration.underline,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      onTap: () {
                                        Navigator.push(context,
                                            geserKiriHalaman(page: Register()));
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Container(
                                  child: ElevatedButton(
                                    child: const Padding(
                                      padding:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      child: Icon(
                                        Icons.qr_code_2_outlined,
                                        size: 40,
                                      ),
                                    ),
                                    onPressed: () async {
                                      // setState(() {});
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => const QrCodeScanner(),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                  'Scan QR Code\n Paket',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
