// ignore_for_file: deprecated_member_use, prefer_const_constructors_in_immutables, sort_child_properties_last, use_build_context_synchronously, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers, avoid_print, unnecessary_new

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:kendali_batu/fotopengawas.dart';
import 'package:kendali_batu/login.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:version_check/version_check.dart';

class ProfilPengawas extends StatefulWidget {
  ProfilPengawas(this.idPeng, this.nama, this.tipe, {Key? key})
      : super(key: key);
  final String idPeng;
  final String nama;
  final String tipe;
  @override
  State<ProfilPengawas> createState() => _ProfilPengawasState();
}

class _ProfilPengawasState extends State<ProfilPengawas> {
  List dataPP = [];

  TextEditingController? namausaha;
  String? _namausaha;
  String? _urlFoto;
  // String urlFoto;
  int? jk;
  String? _jk;
  TextEditingController? nik;
  String? _nik;
  TextEditingController? username;
  String? _username;
  TextEditingController? alamat;
  String? _alamat;
  TextEditingController? nomor;
  String? _nomor;
  TextEditingController? email;
  String? _email;
  TextEditingController? tempatL;
  String? _tempatL;
  TextEditingController? tanggalL;
  String? _tanggalL;
  String? versiLokal;

  @override
  void initState() {
    super.initState();
    // final newVersion = NewVersion(
    //   iOSId: 'com.google.Vespa',
    //   androidId: 'ekenda.sda',
    // );
    // new Timer(const Duration(seconds: 5), _cekUserlogin);
    getProfil(widget.idPeng, context);
    cekVersionLokal();
  }

  cekVersionLokal() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      versiLokal = packageInfo.version;
    });
    // print(packageInfo.version);
    advancedStatusCheck();
  }

  advancedStatusCheck() async {
    // PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final versionCheck = VersionCheck(
      // iOSId: 'com.google.Vespa',
      packageVersion: versiLokal,
      packageName: 'ekenda.sda',
      showUpdateDialog: customShowUpdateDialog,
    );
    await versionCheck.checkVersion(context);

    // print(packageInfo.appName);
    // print(packageInfo.packageName);
    // print(packageInfo.version);
    // print(packageInfo.buildNumber);
  }

  void customShowUpdateDialog(BuildContext context, VersionCheck versionCheck) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: const Text('Pembaruan aplikasi telah tersedia !'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                  'Segera perbarui aplikasi dari versi ${versionCheck.packageVersion}'),
              Text('ke versi ${versionCheck.storeVersion}'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Perbarui'),
            onPressed: () async {
              await versionCheck.launchStore();
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: const Text('Tutup'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  // ignore: missing_return
  void getProfil(idpeng, BuildContext context) async {
    EasyLoading.show();
    const urlProfil =
        "http://ekenda.sidoarjokab.go.id/settingandroid/getprofilpengawas.php";
    // final urlProfil = "http://10.0.2.2/settingandroid/getprofilpengawas.php";

    final response =
        await http.post(Uri.parse(urlProfil), body: {'idpengawas': idpeng});
    EasyLoading.dismiss();
    // final resBody = json.decode(response.body);
    setState(() {
      dataPP = json.decode(response.body);
      _namausaha = dataPP.map((e) => e['badanusaha']).toString();
      _nik = dataPP.map((e) => e['nik']).toString();
      _username = dataPP.map((e) => e['namapengawas']).toString();
      _alamat = dataPP.map((e) => e['alamat']).toString();
      _nomor = dataPP.map((e) => e['nomor']).toString();
      _email = dataPP.map((e) => e['email']).toString();
      _tempatL = dataPP.map((e) => e['tempatL']).toString();
      _tanggalL = dataPP.map((e) => e['tanggalL']).toString();
      _jk = dataPP.map((e) => e['jk']).toString();
      _urlFoto = dataPP.map((e) => e['fotoktp']).toString().replaceAll(
          RegExp(
            "[()]",
          ),
          "");
      // urlFoto = _urlFoto.replaceAll(from, replace)
      nik = TextEditingController(
          text: _nik!.replaceAll(
              RegExp(
                "[({null})]",
              ),
              ""));
      username = TextEditingController(
          text: _username!.replaceAll(
              RegExp(
                "[({null})]",
              ),
              ""));
      if (_alamat == "(null)") {
        // alamat = '';
        alamat = TextEditingController(
            text: _alamat!.replaceAll(
                RegExp(
                  "[(null)]",
                ),
                ""));
      } else {
        alamat = TextEditingController(
            text: _alamat!.replaceAll(
                RegExp(
                  "[()]",
                ),
                ""));
      }
      if (_tempatL == "(null)") {
        tempatL = TextEditingController(
            text: _tempatL!.replaceAll(
                RegExp(
                  "[(null)]",
                ),
                ""));
      } else {
        tempatL = TextEditingController(
            text: _tempatL!.replaceAll(
                RegExp(
                  "[()]",
                ),
                ""));
      }
      tanggalL = TextEditingController(
          text: _tanggalL!.replaceAll(
              RegExp(
                "[({null})]",
              ),
              ""));
      nomor = TextEditingController(
          text: _nomor!.replaceAll(
              RegExp(
                "[({null})]",
              ),
              ""));
      email = TextEditingController(
          text: _email!.replaceAll(
              RegExp(
                "[({null})]",
              ),
              ""));
      namausaha = TextEditingController(
          text: _namausaha!.replaceAll(
              RegExp(
                "[({null})]",
              ),
              ""));
      if (_jk == "(null)") {
        jk = null;
      } else {
        jk = int.parse(_jk!.replaceAll(
            RegExp(
              "[()]",
            ),
            ""));
      }
    });
  }

  // void whatsAppOpen() async {
  //   // String url = " http://wa.me/08979659163?text=tes";
  //   var number = "+6281252006089";
  //   var whatsappUrlAndroid =
  //       "whatsapp://send?phone=$number&text=Mohon maaf, saya ingin mengajukan pertanyaan terkait aplikasi E-Kenda.";
  //   await canLaunch(whatsappUrlAndroid)
  //       ? launch(whatsappUrlAndroid)
  //       : EasyLoading.showError(
  //           'Tidak dapat membuka whatsapp',
  //           duration: const Duration(seconds: 2),
  //         );
  //   // await FlutterLaunch.launchWhatsapp(phone: "08979659163", message: "Hello");
  // }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => false,
      child: new Scaffold(
        drawer: Drawer(
          elevation: 10,
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                //duration: Duration(seconds: 1),
                margin: const EdgeInsets.only(bottom: 30),
                child: Container(
                  width: 500,
                  decoration: BoxDecoration(
                      //backgroundBlendMode: BlendMode.lighten,
                      borderRadius: BorderRadius.circular(70),
                      color: Colors.grey[800]!.withOpacity(0.3)),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0, bottom: 10),
                        child: Image.asset(
                          'lib/aset/pengawas.png',
                          scale: 13,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(widget.nama,
                            style: const TextStyle(
                                fontFamily: 'Paytone', fontSize: 15)),
                      ),
                    ],
                  ),
                ),
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: ExactAssetImage('lib/aset/logobayang.png'),
                      fit: BoxFit.cover),
                ),
              ),
              ListTile(
                leading: Image.asset(
                  'lib/aset/camerafisik.png',
                  scale: 20,
                ),
                title: const Text(
                  'Input Foto',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          FotoLapangan(widget.idPeng, widget.nama, widget.tipe),
                    ),
                  );
                },
              ),
              ListTile(
                leading: Image.asset(
                  'lib/aset/pengawas.png',
                  scale: 25,
                ),
                title: const Text(
                  'Profil Pengawas',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => ProfilPengawas(
                          widget.idPeng, widget.nama, widget.tipe),
                    ),
                  );
                },
              ),
              // ListTile(
              //   leading: Image.asset(
              //     'lib/aset/helpdesk.png',
              //     scale: 7,
              //   ),
              //   title: const Text(
              //     'Bantuan',
              //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              //   ),
              //   onTap: () {
              //     whatsAppOpen();
              //   },
              // ),
              ListTile(
                leading: Image.asset(
                  'lib/aset/logout.png',
                  scale: 20,
                ),
                title: const Text(
                  'Logout',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                onTap: () {
                  // set up the buttons
                  Widget cancelButton = TextButton(
                    child: const Text("Tidak"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                  Widget continueButton = TextButton(
                    child: const Text("Ya"),
                    onPressed: () async {
                      SharedPreferences preferences =
                          await SharedPreferences.getInstance();
                      preferences.clear();
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(pageBuilder: (BuildContext context,
                              Animation animation,
                              Animation secondaryAnimation) {
                            return const LoginAdmin(
                              title: 'Kendali Kota Batu',
                            );
                          }, transitionsBuilder: (BuildContext context,
                              Animation<double> animation,
                              Animation<double> secondaryAnimation,
                              Widget child) {
                            return new SlideTransition(
                              position: new Tween<Offset>(
                                begin: const Offset(1.0, 0.0),
                                end: Offset.zero,
                              ).animate(animation),
                              child: child,
                            );
                          }),
                          (Route route) => false);
                    },
                  );

                  // set up the AlertDialog
                  AlertDialog alert = AlertDialog(
                    title: const Text("Logout"),
                    content: const Text("Apakah anda yakin ingin logout?"),
                    actions: [
                      cancelButton,
                      continueButton,
                    ],
                  );

                  // show the dialog
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return alert;
                    },
                  );
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Padding(
            padding: const EdgeInsets.only(left: 78.0),
            child: Row(
              children: [
                const Text(
                  'E',
                  style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 20),
                ),
                const Text(
                  ' - KENDA',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            margin: const EdgeInsets.all(20),
            width: 700,
            child: Form(
              child: ListView(
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 30),
                    child: Center(
                        child: Text(
                      'DATA PENGAWAS',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextField(
                        readOnly: true,
                        keyboardType: TextInputType.text,
                        controller: namausaha,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Badan usaha'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextField(
                        readOnly: true,
                        keyboardType: const TextInputType.numberWithOptions(
                          decimal: true,
                        ),
                        controller: nik,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(), hintText: 'NIK'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0, right: 5),
                    child: Container(
                      child: const Text(
                        'NIK digunakan sebagai username saat login, hubungi admin pembangunan jika ingin merubah.',
                        style: TextStyle(color: Colors.red),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextField(
                        readOnly: false,
                        keyboardType: TextInputType.text,
                        controller: username,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Nama lengkap'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextField(
                        readOnly: false,
                        keyboardType: TextInputType.text,
                        controller: tempatL,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Tempat lahir'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextField(
                        readOnly: true,
                        controller: tanggalL,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            suffixIcon: Icon(Icons.calendar_today_outlined),
                            hintText: 'Tanggal lahir'),
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(
                                  1960), //DateTime.now() - not to allow to choose before today.
                              lastDate: DateTime(2101));
                          if (pickedDate != null) {
                            print(
                                pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                            String formattedDate =
                                DateFormat('yyyy-MM-dd').format(pickedDate);
                            print(
                                formattedDate); //formatted date output using intl package =>  2021-03-16
                            //you can implement different kind of Date Format here according to your requirement

                            setState(() {
                              tanggalL!.text =
                                  formattedDate; //set output date to TextField value.
                            });
                          } else {
                            print("Date is not selected");
                          }
                        },
                      ),
                    ),
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Radio(
                          value: 1,
                          groupValue: jk,
                          onChanged: (value) async {
                            setState(() {
                              jk = value;
                            });
                          }),
                      // SizedBox(),
                      const Text('Laki-laki'),
                      Radio(
                          value: 2,
                          groupValue: jk,
                          onChanged: (value) async {
                            setState(() {
                              jk = value;
                            });
                          }),
                      // SizedBox(),
                      const Text('Perempuan'),
                    ],
                  ),

                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextFormField(
                        maxLines: 3,
                        readOnly: false,
                        keyboardType: TextInputType.multiline,
                        controller: alamat,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Alamat lengkap'),
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextField(
                        readOnly: false,
                        keyboardType: TextInputType.number,
                        controller: nomor,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(), hintText: 'Nomor HP'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 5.0, right: 5, bottom: 10),
                    child: Container(
                      child: TextField(
                        readOnly: false,
                        keyboardType: TextInputType.text,
                        controller: email,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(), hintText: 'Email'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.black,
                        border: Border.all(width: 5),
                        shape: BoxShape.rectangle,
                      ),
                      // child: Image.network(
                      //   'https://ekenda.sidoarjokab.go.id' + urlFoto,
                      //   fit: BoxFit.contain,
                      //   filterQuality: FilterQuality.medium,
                      // ),
                      child: _urlFoto == null
                          ? const Text('Belum ada foto')
                          : Image.network(
                              // 'http://10.0.2.2/settingandroid/gambar/pemandangan-alam-matahari.jpg',
                              'https://ekenda.sidoarjokab.go.id/${_urlFoto!}',
                              fit: BoxFit.contain,
                              filterQuality: FilterQuality.high,
                            ),
                    ),
                  ),

                  //////////// tombol simpan ///////////////

                  Container(
                    height: 65,
                    padding: const EdgeInsets.only(
                        top: 10, left: 10, right: 10, bottom: 0.20),
                    child: ElevatedButton(
                      // textColor: Colors.white,
                      // color: Colors.green,
                      child: const Text('Simpan'),
                      onPressed: () {
                        simpan(context);
                      },
                    ),
                  ),
                ],
              ),
              // key: _formkey,
            ),
          ),
        ),
      ),
    );
  }

  Future<String> simpan(BuildContext context) async {
    try {
      var url = Uri.parse(
          "http://ekenda.sidoarjokab.go.id/settingandroid/daftarpengawas.php");
      // var url =
      //     Uri.parse("http://10.0.2.2/settingandroid/updateprofilpengawas.php");

      var request = new http.MultipartRequest("POST", url);

      // fields table data pengawas
      request.fields['idpengawas'] = widget.idPeng;
      request.fields['nama'] = username!.text;
      request.fields['tempatL'] = tempatL!.text;
      request.fields['tanggalL'] = tanggalL!.text;
      request.fields['jk'] = jk.toString();
      request.fields['alamat'] = alamat!.text;
      request.fields['email'] = email!.text;
      request.fields['nomor'] = nomor!.text;
      EasyLoading.show(status: 'loading...');

      // response send request to server
      var response = await request.send();

      if (response.statusCode == 200) {
        EasyLoading.showSuccess(
          'Update data pengawas berhasil!',
          duration: const Duration(seconds: 3),
        );
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) =>
                ProfilPengawas(widget.idPeng, widget.nama, widget.tipe),
          ),
        );
      } else if (response.statusCode == 500) {
        EasyLoading.showError(
          'Update data pengawas gagal!\n! Server sedang sibuk !\nMohon coba beberapa saat lagi',
          duration: const Duration(seconds: 3),
        );
      }
      EasyLoading.dismiss();
    } catch (e) {
      debugPrint("error $e");
      Widget okButton = TextButton(
        child: const Text("OK"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: const Text("Pendaftaran gagal"),
        content: const Text(
            "Semua kolom harus di isi dengan benar!\nAtau hubungi Admin E-Kenda"),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
    return 'success';
  }
}
