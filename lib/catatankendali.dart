// ignore_for_file: prefer_const_constructors_in_immutables, import_of_legacy_library_into_null_safe, library_private_types_in_public_api, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
// import 'package:kendali_android/kendalifoto.dart';

//import 'halamankendali1.dart';

class CatatanKendali extends StatefulWidget {
  final String paket;
  final String getCatatan;
  final String namapkt;

  CatatanKendali(this.paket, this.getCatatan, this.namapkt, {Key? key})
      : super(key: key);
  @override
  _CatatanKendaliState createState() => _CatatanKendaliState();
}

class _CatatanKendaliState extends State<CatatanKendali> {
  TextEditingController? catatan;
  TextEditingController? namapaket;
  //String namaPkt;
  @override
  void initState() {
    catatan = TextEditingController(
        text: widget.getCatatan.replaceAll(
            RegExp(
              "[({})]",
            ),
            ""));
    if (catatan!.text == "null") {
      catatan!.text = "";
    }
    namapaket = TextEditingController(
        text: widget.namapkt.replaceAll(
            RegExp(
              "[({})]",
            ),
            ""));
    if (namapaket!.text == "null") {
      namapaket!.text = "";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          'E - KENDALI',
          style: TextStyle(fontSize: 20),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: ListView(
            children: <Widget>[
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Catatan',
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  readOnly: true,
                  controller: namapaket,
                  maxLines: 2,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Nama Paket',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: catatan,
                  maxLength: 500,
                  maxLines: 5,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Catatan',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                // ignore: deprecated_member_use
                child: ElevatedButton(
                  // textColor: Colors.white,
                  // color: Colors.green,
                  child: Text('Update catatan'),
                  onPressed: () {
                    uploadcatatan(context);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  uploadcatatan(BuildContext context) async {
    try {
      // var url3 = Uri.parse("http://10.0.2.2/androidkendali/updatecatatan.php");
      var url3 = Uri.parse(
          "http://ekenda.sidoarjokab.go.id/androidkendali/updatecatatan.php");

      var request = http.MultipartRequest("POST", url3);

      request.fields['idpkt'] = widget.paket;
      request.fields['catatan'] = catatan!.text;
      EasyLoading.show();
      var response = await request.send();

      if (response.statusCode == 200) {
        // ignore: deprecated_member_use
        Widget okButton = TextButton(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        );

        // set up the AlertDialog
        AlertDialog alert = AlertDialog(
          title: Icon(
            Icons.check_circle_outline,
            color: Colors.green,
            size: 50,
          ),
          content: Center(
            heightFactor: 1,
            child: Text(
              "Upload Foto Sukses",
              style: TextStyle(fontSize: 20),
            ),
          ),
          actions: [
            okButton,
          ],
        );

        // show the dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );
        EasyLoading.dismiss();
      } else {
        // ignore: deprecated_member_use
        Widget okButton = TextButton(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        );

        // set up the AlertDialog
        AlertDialog alert = AlertDialog(
          title: Icon(
            Icons.info_rounded,
            color: Colors.yellow,
            size: 50,
          ),
          content: Center(
            heightFactor: 1,
            child: Text(
              "Server Perbaikan",
              style: TextStyle(fontSize: 20),
            ),
          ),
          actions: [
            okButton,
          ],
        );

        // show the dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );
        EasyLoading.dismiss();
      }
    } catch (e) {
      debugPrint("error $e");
      // ignore: deprecated_member_use
      Widget okButton = TextButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Icon(
          Icons.info_outline_rounded,
          color: Colors.red,
          size: 50,
        ),
        content: Center(
          heightFactor: 1,
          child: Text(
            "Isi semua field",
            style: TextStyle(fontSize: 20),
          ),
        ),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
      EasyLoading.dismiss();
    }
  }
}
