// ignore_for_file: unnecessary_brace_in_string_interps, prefer_adjacent_string_concatenation

import 'dart:convert';

List<BadanUsaha> badanUsahaFromJson(String str) =>
    List<BadanUsaha>.from(json.decode(str).map((e) => BadanUsaha.fromJson(e)));

String badanUsahaToJson(List<BadanUsaha> dataPkt) =>
    json.encode(List<dynamic>.from(dataPkt.map((e) => e.toJson())));

class BadanUsaha {
  String idbu;
  String namausaha;
  String npwp;
  //String dateStart;

  BadanUsaha({
    required this.idbu,
    required this.namausaha,
    required this.npwp,
    //this.dateStart,
  });

  factory BadanUsaha.fromJson(Map<String, dynamic> json) => BadanUsaha(
        idbu: json["id_bu"],
        namausaha: json["nama_usaha"],
        npwp: json["npwp"],
      );

  Map<String, dynamic> toJson() => {
        "id_bu": idbu,
        "nama_usaha": namausaha,
        "npwp": npwp,
        //"dateEnd": dateStart,
      };

  @override
  String toString() {
    return '${idbu} ${namausaha.toLowerCase()}' +
        '${namausaha.toUpperCase()}' '${npwp}';
  }
}
