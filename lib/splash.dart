// ignore_for_file: prefer_const_constructors_in_immutables, sort_child_properties_last, use_build_context_synchronously, prefer_const_constructors

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:kendali_batu/fotopengawas.dart';
import 'package:kendali_batu/kendalifoto.dart';
import 'package:kendali_batu/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CekIslogged extends StatefulWidget {
  CekIslogged({Key? key}) : super(key: key);

  @override
  State<CekIslogged> createState() => _CekIsloggedState();
}

class _CekIsloggedState extends State<CekIslogged> {
  @override
  void initState() {
    super.initState();
    // final newVersion = NewVersion(
    //   iOSId: 'com.google.Vespa',
    //   androidId: 'ekenda.sda',
    // );
    Timer(const Duration(seconds: 5), _cekUserlogin);
  }

  void _cekUserlogin() async {
    try {
      WidgetsFlutterBinding.ensureInitialized();
      SharedPreferences preferences = await SharedPreferences.getInstance();
      // preferences.clear();
      var username = preferences.getString('username');
      var kowil = preferences.getString('kowil');
      var nama = preferences.getString('nama');
      var tipe = preferences.getString('tipe');

      // print(username);
      // print(tipe);
      if (username == null || tipe == null) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => LoginAdmin(
              title: 'Kendali Kota Batu',
            ),
          ),
        );
      } else if (tipe == "6") {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => FotoLapangan(
              kowil!.replaceAll(
                  RegExp(
                    "[({})]",
                  ),
                  ""),
              nama!.replaceAll(
                  RegExp(
                    "[({})]",
                  ),
                  ""),
              tipe.replaceAll(
                  RegExp(
                    "[({})]",
                  ),
                  ""),
            ),
          ),
        );
      } else if (tipe == '5') {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => KendaliFoto(
              nama!.replaceAll(
                  RegExp(
                    "[({})]",
                  ),
                  ""),
              tipe.replaceAll(
                  RegExp(
                    "[({})]",
                  ),
                  ""),
            ),
          ),
        );
      }
    } catch (e) {
      EasyLoading.showInfo(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Image.asset(
          'lib/aset/Kota_Batu.png',
          scale: 7,
          // fit: BoxFit.cover,
        ),
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 18, 125, 68),
        ),
      ),
    );
  }
}
