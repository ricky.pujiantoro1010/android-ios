// ignore_for_file: use_build_context_synchronously, deprecated_member_use, prefer_const_constructors, depend_on_referenced_packages, import_of_legacy_library_into_null_safe, unused_local_variable, prefer_const_literals_to_create_immutables, prefer_interpolation_to_compose_strings, sort_child_properties_last, avoid_print

import 'dart:convert';
import 'dart:io';
import 'package:async/async.dart';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geocoding/geocoding.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart' as geo;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:kendali_batu/login.dart';
// import 'package:kendali_batu/profilpengawas.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:url_launcher/url_launcher.dart';

class FotoLapangan extends StatefulWidget {
  const FotoLapangan(this.idPeng, this.nama, this.tipe, {Key? key})
      : super(key: key);

  final String idPeng;
  final String nama;
  final String tipe;

  @override
  State<FotoLapangan> createState() => _FotoLapanganState();
}

class _FotoLapanganState extends State<FotoLapangan> {
  String? address;
  String? bulan;
  int? countDate;
  String? countRencana;
  List dataMingguAkhir = [];
  String? jam;
  String? lat;
  String? getLat;
  String? long;
  String? getLong;
  String? paket;
  String? skpd;
  String? tanggal;
  String? tempDate1;
  String? tempDate2;
  String? day1;
  String? day2;
  String? uraian_;
  double currentlatitude = 0;
  double currentlongitude = 0;
  TextEditingController? realisasi;
  TextEditingController? target;
  TextEditingController? uraian;
  bool? serviceEnabled;
  double distanceImMeter = 0.0;
  double radius = 5.0;
  geo.Position? posisiFoto;
  File? _imagefix;
  String versiLokal = '';
  String versiToko = '';
  String subaddress = 'Batu';
  http.StreamedResponse? response1;
  http.StreamedResponse? response2;
  DateTime? waktuMulai;
  DateTime waktuSekarang = DateTime.now();
  final _formkey = GlobalKey<FormState>();

  // list variabel

  List dataPK = [];
  List dataPKbyNMe = [];
  List datalatlong = [];
  List dataRT = [];
  List dataTgl = [];
  List dataUrian = [];

  ////////////////////////// url batu /////////////////////////////////////////////
  final urlCountMinggu = "http://103.211.82.84/settingandroidbatu/getweek.php";
  final urlPengawas =
      "http://103.211.82.84/settingandroidbatu/getpengawas_paket_.php";

  final urlTgl = "http://103.211.82.84/settingandroidbatu/gettglmulai.php";
  final urllatlong = "http://103.211.82.84/settingandroidbatu/getlatlong.php";
  final urlUraian = "http://103.211.82.84/settingandroidbatu/geturaian.php";

  @override
  void initState() {
    super.initState();
    cekPermission();
    // getCurrentPosition();
    getPkt(widget.idPeng, context);
    // cekVersionLokal();
  }

  void cekPermission() async {
    PermissionStatus permission =
        await LocationPermissions().checkPermissionStatus();
    if (permission == PermissionStatus.denied) {
      PermissionStatus permission =
          await LocationPermissions().requestPermissions();
      // print(permission);
    } else {
      cekServiceLocation(context);
    }
    //print(permission);
  }

  void cekServiceLocation(context) async {
    ServiceStatus serviceStatus =
        await LocationPermissions().checkServiceStatus();
    // print(serviceStatus);
    if (serviceStatus == ServiceStatus.disabled) {
      Widget okButton = ElevatedButton(
        child: const Text("Open Settings"),
        onPressed: () {
          AppSettings.openLocationSettings();
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Row(
          children: const [
            Icon(
              Icons.location_on,
              color: Color.fromARGB(255, 255, 17, 0),
            ),
            Text("Location"),
          ],
        ),
        content: const Text("Harap hidupkan lokasi"),
        elevation: 4,
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
      //EasyLoading.dismiss();
    }
  }

  Future<String> getPkt(idpeng, BuildContext context) async {
    try {
      EasyLoading.show();
      final res =
          await http.post(Uri.parse(urlPengawas), body: {'idpengawas': idpeng});
      final resBody = json.decode(res.body);
      EasyLoading.dismiss();
      setState(() {
        dataPK = resBody;
      });
      // getUraian(idpeng, context);
      if (resBody.length == 0) {
        EasyLoading.showInfo(
          'Perencanaan target belum terisi/belum 100 %',
          duration: Duration(
            seconds: 3,
          ),
        );
      }
    } catch (e) {
      EasyLoading.showError('Tidak terhubung dengan server\n $e',
          duration: Duration(seconds: 5));
    }
    return "";
  }

  /////////////////////////////////////// get data minggu //////////////////////////////////////

  Future<String> getTgl(idRup, BuildContext context) async {
    try {
      EasyLoading.show();
      final res = await http.post(Uri.parse(urlTgl), body: {'id_rup': idRup});
      final resBody = json.decode(res.body);

      setState(() {
        dataTgl = resBody;
      });

      //print(resBody);
      // print(countDate);
      EasyLoading.dismiss();
      if (resBody.length == 0) {
        EasyLoading.showError(
          'Data tanggal tidak di temukan',
          duration: Duration(seconds: 3),
        );
      }
    } catch (e) {
      EasyLoading.showError(
        '$e',
        duration: Duration(seconds: 3),
      );
    }

    return "success";
  }

  /////////////////////////////////////// get data minggu //////////////////////////////////////

  Future<String> getMingguAkhir(kodeRup, BuildContext context) async {
    try {
      EasyLoading.show();
      final res =
          await http.post(Uri.parse(urlCountMinggu), body: {'idPkt': kodeRup});
      final resBody = json.decode(res.body);

      setState(() {
        dataMingguAkhir = resBody;
        skpd = dataTgl.map((e) => {e['id_satker']}).toString().replaceAll(
            RegExp(
              "[({})]",
            ),
            "");
        ////////////// set jumlah minggu akhir /////////////
        countRencana = dataMingguAkhir
            .map((e) => {e['mingguterakhir']})
            .toString()
            .replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");

        //////////// set waktu mulai perencanaan ///////////
        tempDate1 = dataTgl.map((e) => {e['dateStart']}).toString();
        tempDate2 = dataTgl.map((e) => {e['dateEnd']}).toString();
        waktuMulai = DateTime.parse(
          tempDate1!.replaceAll(
              RegExp(
                "[({})]",
              ),
              ""),
        );

        ///////////////// get hari sekarang //////////////////////

        final day1 = DateFormat('EEEE').format(waktuSekarang);
        final day2 = DateFormat('EEEE').format(waktuMulai!);

        ///////////////////// hitung jarak hari (untuk menentukan minggu) //////////////

        final difference = waktuSekarang.difference(waktuMulai!).inDays;
        final hasil = difference / 7;
        final hasil1 = (hasil).toDouble();
        final finalHasil = hasil1.ceil();

        ///////////////////// print hasil //////////////////
        print(difference);
        print(waktuMulai);
        print(finalHasil);
        print(day1);
        print(day2);

        ////////////////////// cek kondisi tanggal terkini & set minggu //////////////////

        if (finalHasil == 0) {
          countDate = 1;
        } else if (finalHasil > int.parse(countRencana!)) {
          countDate = int.parse(
            countRencana!.replaceAll(
                RegExp(
                  "[({})]",
                ),
                ""),
          );
        } else {
          // if (day1 == day2) {
          //   countDate = finalHasil + 1;
          // } else {
          countDate = finalHasil;
          // }
        }
      });
      EasyLoading.dismiss();

      if (resBody.length == 0) {
        EasyLoading.showInfo('Data minggu tidak ditemukan');
      }
      print(paket);
      print(paket! + countDate.toString());
      getUraian(paket! + countDate.toString(), context);
    } catch (e) {
      EasyLoading.showError(
        '$e',
        duration: Duration(seconds: 3),
      );
    }

    return "success";
  }

  Future<void> getUraian(idRup, BuildContext context) async {
    try {
      EasyLoading.show();
      final res =
          await http.post(Uri.parse(urlUraian), body: {'id_perencana': idRup});
      final resBody = json.decode(res.body);

      setState(() {
        dataUrian = resBody;
        uraian_ = dataUrian.map((e) => {e['uraian']}).toString();
        uraian = TextEditingController(
            text: uraian_!.replaceAll(
                RegExp(
                  "[({})]",
                ),
                ""));
      });
      EasyLoading.dismiss(animation: true);
    } catch (e) {
      EasyLoading.showInfo(
        '$e',
        duration: Duration(seconds: 3),
      );
      print(e);
    }
  }

  /////////////// proses ambil foto ///////////////////
  PickedFile? pickedFile;
  Future getImageFromCamera(context) async {
    final picker = ImagePicker();
    EasyLoading.show();
    final pickedFile = await picker.pickImage(
        source: ImageSource.camera,
        imageQuality: 50,
        preferredCameraDevice: CameraDevice.rear);
    if (pickedFile == null) {
      EasyLoading.showError('Gagal mengambil gambar');
    } else {
      setState(() {
        _imagefix = File(pickedFile.path);
      });
    }
    EasyLoading.dismiss();
  }

  ////////////// proses logout //////////////

  logout(BuildContext context) async {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: const Text("Tidak"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: const Text("Ya"),
      onPressed: () async {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.clear();
        Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(pageBuilder: (BuildContext context,
                Animation animation, Animation secondaryAnimation) {
              return LoginAdmin(
                title: 'Kendali Kota Batu',
              );
            }, transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              return SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(1.0, 0.0),
                  end: Offset.zero,
                ).animate(animation),
                child: child,
              );
            }),
            (Route route) => false);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Logout"),
      content: const Text("Apakah anda yakin ingin logout?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  ////////////////// porses ambil lokasi /////////////////

  Future<void> getCurrentPosition() async {
    try {
      // proses ambil latitude & longitude //
      geo.Position position = await geo.Geolocator.getCurrentPosition(
          desiredAccuracy: geo.LocationAccuracy.high);
      setState(() {
        posisiFoto = position;
      });

      // proses ambil alamat //

      List<Placemark> placemarks = await placemarkFromCoordinates(
          posisiFoto!.latitude, posisiFoto!.longitude);

      Placemark place = placemarks[0];

      /////////// tanggal /////////////////

      final DateFormat formatter = DateFormat('dd-MM-yyyy');
      final String formatted = formatter.format(waktuSekarang);

      ///////////// jam /////////////////

      final DateFormat jamformatter = DateFormat('HH:mm:ss');
      final String jamformatted = jamformatter.format(waktuSekarang);

      setState(() {
        ///////////////// get hari sekarang //////////////////////

        day1 = DateFormat('EEEE').format(waktuSekarang);
        day2 = DateFormat('EEEE').format(waktuMulai!);
        ///////////// set posisi & alamat /////////////

        lat = posisiFoto!.latitude.toString();
        long = posisiFoto!.longitude.toString();
        address =
            "${place.thoroughfare} | ${place.subThoroughfare} | ${place.locality} | ${place.subAdministrativeArea} | ${place.administrativeArea} | ${place.postalCode}";

        /////////// set jam dan tanggal ////////////////
        tanggal = formatted;
        jam = jamformatted;
      });
      // print(countDate);
    } catch (e) {
      // print(e);
      EasyLoading.showError(
        e.toString(),
        duration: Duration(seconds: 3),
      );
    }
  }

  ///////////////////////////////// inisialisasi variabel image ///////////////////////////////////////////////////////////////
  upload(BuildContext context) async {
    try {
      ////////////////////////////////////  url upload dan update perencanaan /////////////////////////////////////////////////
// url batu
      var url1 =
          Uri.parse("http://103.211.82.84/settingandroidbatu/uploadimg.php");
      var urlupdate =
          Uri.parse("http://103.211.82.84/settingandroidbatu/updatelokasi.php");
      // var url1 = Uri.parse(
      //     "http://ekenda.sidoarjokab.go.id/settingandroid/uploadimg.php");
      // var urlupdate = Uri.parse(
      //     "http://ekenda.sidoarjokab.go.id/settingandroid/updatelokasi.php");
      // var url1 = Uri.parse("http://10.0.2.2/settingandroid/uploadimg.php");
      // var urlupdate =
      // Uri.parse("http://10.0.2.2/settingandroid/updatelokasi.php");

      ////////////////////////////////// Method Kirim data /////////////////////////////////////////////////////////////////////////

      var request1 = http.MultipartRequest("POST", url1);
      var request2 = http.MultipartRequest("POST", urlupdate);

      ////////////////////////////// definisi fields yg akan di inputkan ////////////////////////////////////////////////////////////
      // if (address.contains(subaddress)) {
      request1.fields['idpkt'] = paket! + countDate.toString();
      request1.fields['ukid'] = skpd!;
      request1.fields['lat'] = lat!;
      request1.fields['lng'] = long!;
      request1.fields['address'] = address!;
      request1.fields['minggu'] = countDate.toString();
      request1.fields['usertipe'] = widget.tipe.toString();

      request2.fields['idpkt'] = paket!;
      request2.fields['lang'] = lat!;
      request2.fields['long'] = long!;
      request2.fields['address'] = address!;
      // } else {
      //   request1.fields['idpkt'] = paket + countDate.toString();
      //   request1.fields['ukid'] = skpd;
      //   request1.fields['lat'] = lat;
      //   request1.fields['lng'] = long;
      //   request1.fields['address'] = address;
      //   request1.fields['minggu'] = countDate.toString();
      //   request1.fields['usertipe'] = widget.tipe.toString();
      // }

      var stream =
          http.ByteStream(DelegatingStream.typed(_imagefix!.openRead()));
      var length = await _imagefix!.length();
      var multipartFile = http.MultipartFile("image", stream, length,
          filename: _imagefix!.path);
      request1.files.add(multipartFile);
      // request1.finalize();
      EasyLoading.show(status: 'loading...');
      ///////////////////////// kirim response ///////////////////////////////

      response1 = await request1.send();
      response2 = await request2.send();

      print(response1!.contentLength);
      print(response2!.contentLength);
      //////////////////////// warning untuk kirim data //////////////////////
      if (response1!.statusCode == 200 || response1!.statusCode == 0) {
        EasyLoading.showSuccess(
          'Upload Success',
          duration: Duration(seconds: 3),
        );
        setState(() {
          imageCache.clear();
          _imagefix = null;
        });
      } else if (response1!.statusCode == 403 || response1!.statusCode == 400) {
        EasyLoading.showError(
          'Gagal Upload/Bad response',
          duration: Duration(seconds: 3),
        );
      } else if (response1!.statusCode == 500) {
        EasyLoading.showError(
          'Server sedang perbaikan',
          duration: Duration(seconds: 5),
        );
      }
      EasyLoading.dismiss();
    } catch (e) {
      debugPrint("error $e");
      EasyLoading.showInfo('$e');
    }
    EasyLoading.dismiss();
  }

  // void whatsAppOpen() async {
  //   // String url = " http://wa.me/08979659163?text=tes";
  //   var number = "+6281252006089";
  //   var whatsappUrlAndroid =
  //       "whatsapp://send?phone=$number&text=Mohon maaf, saya ingin mengajukan pertanyaan terkait aplikasi E-Kenda.";
  //   await canLaunch(whatsappUrlAndroid)
  //       ? launch(whatsappUrlAndroid)
  //       : EasyLoading.showError(
  //           'Tidak dapat membuka whatsapp',
  //           duration: const Duration(seconds: 2),
  //         );
  //   // await FlutterLaunch.launchWhatsapp(phone: "08979659163", message: "Hello");
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        drawer: Drawer(
          elevation: 10,
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                //duration: Duration(seconds: 1),
                margin: EdgeInsets.only(bottom: 30),
                decoration: BoxDecoration(
                    // image: DecorationImage(
                    //     image: ExactAssetImage('lib/aset/logobayang.png'),
                    //     fit: BoxFit.cover),
                    ),
                child: Container(
                  width: 500,
                  decoration: BoxDecoration(
                      //backgroundBlendMode: BlendMode.lighten,
                      borderRadius: BorderRadius.circular(70),
                      color: Colors.grey[800]!.withOpacity(0.3)),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0, bottom: 10),
                        child: Image.asset(
                          'lib/aset/pengawas.png',
                          scale: 13,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(widget.nama,
                            style:
                                TextStyle(fontFamily: 'Paytone', fontSize: 15)),
                      ),
                    ],
                  ),
                ),
              ),
              ListTile(
                leading: Image.asset(
                  'lib/aset/camerafisik.png',
                  scale: 20,
                ),
                title: Text(
                  'Input Foto',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          FotoLapangan(widget.idPeng, widget.nama, widget.tipe),
                    ),
                  );
                },
              ),
              // ListTile(
              //   leading: Image.asset(
              //     'lib/aset/pengawas.png',
              //     scale: 25,
              //   ),
              //   title: Text(
              //     'Profil Pengawas',
              //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              //   ),
              //   onTap: () {
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //         builder: (BuildContext context) => ProfilPengawas(
              //             widget.idPeng, widget.nama, widget.tipe),
              //       ),
              //     );
              //   },
              // ),
              // ListTile(
              //   leading: Image.asset(
              //     'lib/aset/helpdesk.png',
              //     scale: 7,
              //   ),
              //   title: Text(
              //     'Bantuan (Admin E-kenda)',
              //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              //   ),
              //   onTap: () {
              //     whatsAppOpen();
              //   },
              // ),
              ListTile(
                leading: Image.asset(
                  'lib/aset/logout.png',
                  scale: 20,
                ),
                title: Text(
                  'Logout',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                onTap: () {
                  // set up the buttons
                  Widget cancelButton = TextButton(
                    child: Text("Tidak"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                  Widget continueButton = TextButton(
                    child: Text("Ya"),
                    onPressed: () async {
                      SharedPreferences preferences =
                          await SharedPreferences.getInstance();
                      preferences.clear();
                      Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(pageBuilder: (BuildContext context,
                              Animation animation,
                              Animation secondaryAnimation) {
                            return LoginAdmin(
                              title: 'Kendali Kota Batu',
                            );
                          }, transitionsBuilder: (BuildContext context,
                              Animation<double> animation,
                              Animation<double> secondaryAnimation,
                              Widget child) {
                            return SlideTransition(
                              position: Tween<Offset>(
                                begin: const Offset(1.0, 0.0),
                                end: Offset.zero,
                              ).animate(animation),
                              child: child,
                            );
                          }),
                          (Route route) => false);
                    },
                  );

                  // set up the AlertDialog
                  AlertDialog alert = AlertDialog(
                    title: Text("Logout"),
                    content: Text("Apakah anda yakin ingin logout?"),
                    actions: [
                      cancelButton,
                      continueButton,
                    ],
                  );

                  // show the dialog
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return alert;
                    },
                  );
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Colors.green[700],
          title: Padding(
            padding: const EdgeInsets.only(left: 78.0),
            child: Row(
              children: [
                Text(
                  'E',
                  style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 20),
                ),
                Text(
                  ' - KENDA',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            margin: EdgeInsets.all(20),
            width: 700,
            child: Form(
              //autovalidate: true,
              key: _formkey,
              //autovalidate: true,
              child: ListView(
                children: <Widget>[
                  //////////////////////////////////// Pilih Paket //////////////////////////////////////////////////////////

                  // Padding(
                  //   padding: const EdgeInsets.only(
                  //       top: 20.0, left: 15.0, bottom: 10),
                  //   child: Text(
                  //     "Paket : ",
                  //     style: TextStyle(fontSize: 20),
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20, bottom: 20),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40.0),
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 20, left: 20),
                        child: DropdownButton(
                          itemHeight: null,
                          isExpanded: true,
                          hint: Row(
                            children: [
                              Text(
                                'Pilih Paket ',
                              ),
                              Text(
                                '*',
                                style: TextStyle(color: Colors.red),
                              )
                            ],
                          ),
                          value: paket,
                          items:
                              // dataPK.where((e) => e['show'] == true).map((item) {
                              dataPK.map((item) {
                            return DropdownMenuItem(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(item['pkt_nama_paket'] +
                                    " ( Rp. " +
                                    item['pagu'] +
                                    " )"),
                              ),
                              value: item['pkt_id'],
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              paket = value as String?;
                              imageCache.clear();
                            });
                            getTgl(paket, context);
                            getMingguAkhir(paket, context);
                          },
                        ),
                      ),
                    ),
                  ),
                  // Text("kend_id :  $paket"),

                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20),
                        child: TextFormField(
                          readOnly: true,
                          maxLines: 3,
                          // expands: true,
                          controller: uraian,
                          decoration: InputDecoration(
                              border: InputBorder.none, hintText: 'Uraian'),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                            child: Icon(Icons.camera_alt),
                            onPressed: () async {
                              if (serviceEnabled == false) {
                                Widget okButton = TextButton(
                                  child: Text("OK"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    setState(() {
                                      serviceEnabled = true;
                                    });
                                  },
                                );

                                // set up the AlertDialog
                                AlertDialog alert = AlertDialog(
                                  title: Text("Lokasi tidak terdeteksi!"),
                                  content: Text("Lokasi mohon di hidupkan!"),
                                  actions: [
                                    okButton,
                                  ],
                                );

                                // show the dialog
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return alert;
                                  },
                                );
                              } else if (paket == null) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text(
                                          'Mohon pilih paket pekerjaan terlebih dahulu !')),
                                );
                              } else {
                                getImageFromCamera(context);
                                getCurrentPosition();
                              }
                            }),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Center(
                      child: _imagefix == null
                          ? Container(
                              width: 350,
                              height: 200,
                              color: Colors.grey[200],
                              child: SizedBox(
                                child: Center(
                                  child: Text('Belum ada gambar !'),
                                ),
                              ),
                            )
                          : Stack(
                              children: <Widget>[
                                Image.file(_imagefix!),
                                Container(
                                  height: 300,
                                  alignment: Alignment.centerRight,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Text(
                                          "Lat : " + lat! + ", Long : " + long!,
                                          style: TextStyle(
                                              backgroundColor: Colors.white70,
                                              fontSize: 15),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 3.0),
                                            child: Text(
                                              "Tanggal : " +
                                                  tanggal! +
                                                  " Pukul : ",
                                              style: TextStyle(
                                                  backgroundColor:
                                                      Colors.white70,
                                                  fontSize: 15),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 3.0),
                                            child: Text(
                                              jam!,
                                              style: TextStyle(
                                                  backgroundColor:
                                                      Colors.white70,
                                                  fontSize: 15),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 3.0),
                                        child: Text(
                                          "Alamat : " + address!,
                                          style: TextStyle(
                                              backgroundColor: Colors.white70,
                                              fontSize: 15),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                    ),
                  ),
                  //Text(_imagefix.toString()),
                  //////////// tombol upload ///////////////

                  Container(
                    height: 65,
                    padding: EdgeInsets.only(
                        top: 10, left: 10, right: 10, bottom: 0.20),
                    child: ElevatedButton(
                      // textColor: Colors.white,
                      // color: Colors.green,
                      child: Text('Upload'),
                      onPressed: () {
                        upload(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
