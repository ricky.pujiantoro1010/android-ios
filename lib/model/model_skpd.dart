// ignore_for_file: prefer_adjacent_string_concatenation, unnecessary_string_interpolations, unnecessary_this

import 'dart:convert';

List<Satker> satkerFromJson(String str) =>
    List<Satker>.from(json.decode(str).map((x) => Satker.fromJson(x)));

String satker(List<Satker> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Satker {
  String idpd;
  String namapd;

  Satker({
    required this.idpd,
    required this.namapd,
  });

  factory Satker.fromJson(Map<String, dynamic> json) => Satker(
        idpd: json["psk_uk_id"],
        namapd: json["psk_nama"],
      );

  Map<String, dynamic> toJson() => {
        "psk_uk_id": idpd,
        "psk_nama": namapd,
      };

  @override
  String toString() {
    return '${this.idpd} ${this.namapd.toLowerCase()}' +
        '${this.namapd.toUpperCase()}';
  }
}
