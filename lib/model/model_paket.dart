// ignore_for_file: unnecessary_this, unnecessary_string_interpolations, prefer_adjacent_string_concatenation

import 'dart:convert';

List<Paket> paketFromJson(String str) =>
    List<Paket>.from(json.decode(str).map((e) => Paket.fromJson(e)));

String paketToJson(List<Paket> dataPkt) =>
    json.encode(List<dynamic>.from(dataPkt.map((e) => e.toJson())));

class Paket {
  String pktId;
  String pktNamaPaket;
  String dateStart;

  Paket({
    required this.pktId,
    required this.pktNamaPaket,
    required this.dateStart,
  });

  factory Paket.fromJson(Map<String, dynamic> json) => Paket(
        pktId: json["pkt_id"],
        pktNamaPaket: json["pkt_nama_paket"],
        dateStart: json['dateEnd'],
      );

  Map<String, dynamic> toJson() => {
        "pkt_id": pktId,
        "pkt_nama_paket": pktNamaPaket,
        "dateEnd": dateStart,
      };

  @override
  String toString() {
    return '${this.pktId} ${this.pktNamaPaket.toLowerCase()}' +
        '${this.pktNamaPaket.toUpperCase()}';
  }
}
