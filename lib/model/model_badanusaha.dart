// ignore_for_file: unnecessary_this, duplicate_ignore

import 'dart:convert';

List<BadanUsaha> badanUsahaFromJson(String str) =>
    List<BadanUsaha>.from(json.decode(str).map((e) => BadanUsaha.fromJson(e)));

String badanUsahaToJson(List<BadanUsaha> dataPkt) =>
    json.encode(List<dynamic>.from(dataPkt.map((e) => e.toJson())));

class BadanUsaha {
  String? idbu;
  String? namausaha;
  String? npwp;
  //String dateStart;

  BadanUsaha({
    this.idbu,
    this.namausaha,
    this.npwp,
    //this.dateStart,
  });

  factory BadanUsaha.fromJson(Map<String, dynamic> json) => BadanUsaha(
        idbu: json["id_bu"],
        namausaha: json["nama_usaha"],
        npwp: json["npwp"],
      );

  Map<String, dynamic> toJson() => {
        "id_bu": idbu,
        "nama_usaha": namausaha,
        "npwp": npwp,
        //"dateEnd": dateStart,
      };

  @override
  String toString() {
    // ignore: unnecessary_this
    return '${this.idbu} ${this.namausaha!.toLowerCase()}'
        '${this.namausaha!.toUpperCase()}'
        '${this.npwp}';
  }
}
