// ignore_for_file: import_of_legacy_library_into_null_safe, library_private_types_in_public_api, prefer_const_literals_to_create_immutables, prefer_const_constructors, avoid_print, annotate_overrides, prefer_interpolation_to_compose_strings

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:kendali_batu/login.dart';
import 'package:kendali_batu/scanqrcode.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class InformasiPaket extends StatefulWidget {
  const InformasiPaket({required this.idrup, Key? key}) : super(key: key);

  final String idrup;

  @override
  _InformasiPaketState createState() => _InformasiPaketState();
}

class _InformasiPaketState extends State<InformasiPaket> {
// variable global
  List totalrealisasi = [];
  List totalminggu = [];
  List tglkontrak = [];
  List inputterakhir = [];
  List tglcetaklist = [];
  List tglveriflist = [];
  List tglperpanjangan = [];
  List tglbktfisik = [];
  String _tglbktfsk = '';
  String _tglverif = '';
  String _tmptglverif = '';
  String _tmpperpanjangan = '';
  String perpanj = '';
  String _tglcetak = '';
  String _tmptglcetak = '';
  String lastinput = '';
  String tmplastinput = '';
  String stringpercent = '';
  String tempstringpercent = '';
  String _totalminggu = '';
  String _tmptotalminggu = '';
  String _tmptglkontrak = '';
  String _tglbast = '';
  String _tmptglbast = '';
  String _tglkontrak = '';
  String _tglakhirkontrak = '';
  String namapkt = '';
  String tempnamapkt = '';
  String tmpstatuspkt = '';
  String statuspkt = '';
  int percent = 0;
  double tmppercent = 0;

  // url base lokal

  // final urltotalrealisasi = "http://10.0.2.2/androidkendali/totalrealisasi.php";
  // final urltotalminggu = "http://10.0.2.2/androidkendali/totalminggu.php";
  // final urltanggalmulaikontrak =
  //     "http://10.0.2.2/androidkendali/tanggalkontrak.php";
  // final urlinputrealisasiterakhir =
  //     "http://10.0.2.2/androidkendali/inputrealisasiterakhir.php";
  // final urltglctk = "http://10.0.2.2/androidkendali/tanggalcetak.php";
  // final urltglverif = "http://10.0.2.2/androidkendali/tanggalverif.php";

  // url server

  final urltotalrealisasi =
      "http://103.211.82.84/settingandroidbatu/totalrealisasi.php";
  final urltotalminggu =
      "http://103.211.82.84/settingandroidbatu/totalminggu.php";
  final urltanggalmulaikontrak =
      "http://103.211.82.84/settingandroidbatu/tanggalkontrak.php";
  final urlinputrealisasiterakhir =
      "http://103.211.82.84/settingandroidbatu/inputrealisasiterakhir.php";
  final urltglctk = "http://103.211.82.84/settingandroidbatu/tanggalcetak.php";
  final urltglverif =
      "http://103.211.82.84/settingandroidbatu/tanggalverif.php";
  final urltglperpanjangan =
      "http://103.211.82.84/settingandroidbatu/perpanjangan.php";
  final urltglbktfskakhir =
      "http://103.211.82.84/settingandroidbatu/tanggalfototerakhir.php";
  // final urltotalrealisasi =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/totalrealisasi.php";
  // final urltotalminggu =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/totalminggu.php";
  // final urltanggalmulaikontrak =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/tanggalkontrak.php";
  // final urlinputrealisasiterakhir =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/inputrealisasiterakhir.php";
  // final urltglctk =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/tanggalcetak.php";
  // final urltglverif =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/tanggalverif.php";
  // final urltglperpanjangan =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/perpanjangan.php";
  // final urltglbktfskakhir =
  //     "http://ekenda.sidoarjokab.go.id/settingandroid/tanggalfototerakhir.php";

  void totalRealisasi(idrup) async {
    EasyLoading.show();
    final res =
        await http.post(Uri.parse(urltotalrealisasi), body: {'idrup': idrup});
    totalrealisasi = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      tempstringpercent = totalrealisasi
          .map((e) => {e['TotalRealisasi']})
          .toString()
          .replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });

    // seleksi data
    if (tempstringpercent == 'null') {
      // EasyLoading.showError('Data tidak ditemukan!');
      percent = 0;
      stringpercent = '';
    } else {
      stringpercent = totalrealisasi
          .map((e) => {e['TotalRealisasi']})
          .toString()
          .replaceAll(
              RegExp(
                "[({})]",
              ),
              "");

      tmppercent = double.parse(stringpercent);
      percent = tmppercent.round();
      if (percent > 100 && mounted) {
        setState(() {
          stringpercent = "100";
          percent = 100;
        });
      }
      print(percent);
    }
  }

  void totalMinggu(idrup) async {
    EasyLoading.show();
    final res =
        await http.post(Uri.parse(urltotalminggu), body: {'idrup': idrup});
    totalminggu = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      _tmptotalminggu =
          totalminggu.map((e) => {e['totalminggu']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });

    // seleksi data
    if (_tmptotalminggu == 'null') {
      _totalminggu = 'Data tidak ditemukan';
    } else {
      _totalminggu =
          totalminggu.map((e) => {e['totalminggu']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    }
  }

  void inputlastrealisasi(idrup) async {
    EasyLoading.show();
    final res = await http
        .post(Uri.parse(urlinputrealisasiterakhir), body: {'idrup': idrup});
    inputterakhir = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      tmplastinput =
          inputterakhir.map((e) => {e['tglterakhir']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });

    // seleksi data
    if (tmplastinput == 'null') {
      lastinput = 'Data tidak ditemukan';
    } else {
      lastinput =
          inputterakhir.map((e) => {e['tglterakhir']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    }
  }

  void tglmulaikontrak(idrup) async {
    EasyLoading.show();
    final res = await http
        .post(Uri.parse(urltanggalmulaikontrak), body: {'idrup': idrup});
    tglkontrak = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      _tmptglkontrak =
          tglkontrak.map((e) => {e['tgl_awal_kontrak']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
      _tmptglbast =
          tglkontrak.map((e) => {e['tgl_bast']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
      tempnamapkt =
          tglkontrak.map((e) => {e['nama_paket']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
      tmpstatuspkt =
          tglkontrak.map((e) => {e['status_paket']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });

    // seleksi data
    if (_tmptglkontrak == 'null' && tempnamapkt == 'null') {
      setState(() {
        _tglkontrak = 'Data tidak ditemukan';
        _tglakhirkontrak = 'Data tidak ditemukan';
        _tglbast = 'Data tidak ditemukan';
        namapkt = 'Nama paket tidak ditemukan';
      });
    } else if (tempnamapkt == 'null') {
      setState(() {
        namapkt = 'Nama paket tidak ditemukan';
        _tglkontrak = tglkontrak
            .map((e) => {e['tgl_awal_kontrak']})
            .toString()
            .replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");
        _tglakhirkontrak = tglkontrak
            .map((e) => {e['tgl_akhir_kontrak']})
            .toString()
            .replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");
        _tglbast = tglkontrak.map((e) => {e['tgl_bast']}).toString().replaceAll(
            RegExp(
              "[({})]",
            ),
            "");
      });
    } else if (_tmptglkontrak == 'null') {
      setState(() {
        _tglkontrak = 'Belum melakukan kontrak';
        _tglakhirkontrak = 'Belum melakukan kontrak';
        _tglbast = 'Belum melakukan kontrak';

        namapkt =
            tglkontrak.map((e) => {e['nama_paket']}).toString().replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");
      });
    } else if (_tmptglbast == 'null') {
      setState(() {
        _tglbast = 'Belum upload BAST';
        _tglkontrak = tglkontrak
            .map((e) => {e['tgl_awal_kontrak']})
            .toString()
            .replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");
        _tglakhirkontrak = tglkontrak
            .map((e) => {e['tgl_akhir_kontrak']})
            .toString()
            .replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");
        namapkt =
            tglkontrak.map((e) => {e['nama_paket']}).toString().replaceAll(
                RegExp(
                  "[({})]",
                ),
                "");
      });
    } else {
      _tglkontrak =
          tglkontrak.map((e) => {e['tgl_awal_kontrak']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
      _tglakhirkontrak =
          tglkontrak.map((e) => {e['tgl_akhir_kontrak']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
      _tglbast = tglkontrak.map((e) => {e['tgl_bast']}).toString().replaceAll(
          RegExp(
            "[({})]",
          ),
          "");
      if (tmpstatuspkt == 'null') {
        setState(() {
          statuspkt = 'Belum di verifikasi oleh PPkom';
        });
      } else if (tmpstatuspkt == '1') {
        setState(() {
          statuspkt = '* Paket dengan Termin';
        });
      } else if (tmpstatuspkt == '3') {
        setState(() {
          statuspkt = '';
        });
      }
      namapkt = tglkontrak.map((e) => {e['nama_paket']}).toString().replaceAll(
          RegExp(
            "[({})]",
          ),
          "");
    }
  }

  void tglcetak(idrup) async {
    EasyLoading.show();
    final res = await http.post(Uri.parse(urltglctk), body: {'idrup': idrup});
    tglcetaklist = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      _tmptglcetak =
          tglcetaklist.map((e) => {e['tglcetak']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });

    // seleksi data
    if (_tmptglcetak == "null") {
      setState(() {
        _tglcetak = 'Belum cetak';
      });
    } else {
      _tglcetak =
          tglcetaklist.map((e) => {e['tglcetak']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    }
  }

  void tglverif(idrup) async {
    EasyLoading.show();
    final res = await http.post(Uri.parse(urltglverif), body: {'idrup': idrup});
    tglveriflist = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      _tmptglverif =
          tglveriflist.map((e) => {e['tgl_verifikasi']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });

    // seleksi data
    if (_tmptglverif == 'null') {
      setState(() {
        _tglverif = 'Belum dilakukan verifikasi';
      });
    } else {
      _tglverif =
          tglveriflist.map((e) => {e['tgl_verifikasi']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    }
  }

  void perpanjangan(idrup) async {
    EasyLoading.show();
    final res =
        await http.post(Uri.parse(urltglperpanjangan), body: {'idrup': idrup});
    tglperpanjangan = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      _tmpperpanjangan =
          tglperpanjangan.map((e) => {e['perpanjangan']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });

    // seleksi data
    if (_tmpperpanjangan == "null" || _tmpperpanjangan == _tglakhirkontrak) {
      perpanj = 'Tidak melakukan perpanjangan';
    } else {
      perpanj = 'Melakukan perpanjangan';
    }
  }

  void tglakhirbktfsk(idrup) async {
    EasyLoading.show();
    final res =
        await http.post(Uri.parse(urltglbktfskakhir), body: {'idrup': idrup});
    tglbktfisik = json.decode(res.body);
    EasyLoading.dismiss();

    setState(() {
      _tglbktfsk =
          tglbktfisik.map((e) => {e['tgl_bkt_fsk']}).toString().replaceAll(
              RegExp(
                "[({})]",
              ),
              "");
    });
  }

  void initState() {
    super.initState();
    // state server

    totalRealisasi(widget.idrup);
    tglmulaikontrak(widget.idrup);
    totalMinggu(widget.idrup);
    inputlastrealisasi(widget.idrup);
    tglverif(widget.idrup);
    tglcetak(widget.idrup);
    perpanjangan(widget.idrup);
    tglakhirbktfsk(widget.idrup);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          child: Icon(Icons.qr_code),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => QrCodeScanner(),
              ),
            );
          },
        ),
        title: Padding(
          padding: const EdgeInsets.only(left: 78.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'E',
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                    fontSize: 20),
              ),
              Text(
                ' - KENDA',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 19),
              ),
            ],
          ),
        ),
        actions: [
          GestureDetector(
              child: Padding(
                padding: const EdgeInsets.only(right: 18.0),
                child: Icon(Icons.login_rounded),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => LoginAdmin(
                      title: '',
                    ),
                  ),
                );
              })
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.green[50],
          // image: DecorationImage(
          //     image: ExactAssetImage('lib/aset/logobayang1.png'),
          //     fit: BoxFit.cover),
        ),
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 5, right: 5, bottom: 10, top: 20),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(13),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 17),
                        blurRadius: 17,
                        spreadRadius: -23,
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: [
                        Text(
                          namapkt,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        // Spacer(),
                        Text(
                          statuspkt,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: GridView.count(
                //widget yang akan ditampilkan dalam 1 baris adalah 2
                crossAxisCount: 2,
                children: [
                  //card ditampilkan disini
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: CircularPercentIndicator(
                                  radius: 100.0,
                                  lineWidth: 20.0,
                                  animation: true,
                                  percent: percent.round() / 100,
                                  center: FittedBox(
                                    fit: BoxFit.contain,
                                    child: Text(
                                      percent.toString() + "%",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            // Spacer(),
                            // Center(child: Text(stringpercent + '%')),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Text(
                                  'Total Realisasi',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Spacer(),
                            // Center(
                            //   child: Image.asset(
                            //     'lib/aset/calendar.gif',
                            //     scale: 5,
                            //   ),
                            // ),
                            // Spacer(),
                            Center(
                              child: Text(
                                lastinput,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Spacer(),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Center(
                                child: FittedBox(
                                  child: Text(
                                    'Input terakhir Realisasi',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 30),
                            //     child: Image.asset(
                            //       'lib/aset/handshake.gif',
                            //       scale: 5,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                                child: Text(
                              _tglkontrak,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: FittedBox(
                                  child: Text(
                                    'Tanggal mulai kontrak',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 30),
                            //     child: Image.asset(
                            //       'lib/aset/handshake.gif',
                            //       scale: 5,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                                child: Text(
                              _tglakhirkontrak,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: FittedBox(
                                  child: Text(
                                    'Tanggal akhir kontrak',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 20.0),
                            //     child: Image.asset(
                            //       'lib/aset/sun.gif',
                            //       scale: 4,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                              child: Text(
                                _totalminggu,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Text(
                                  'Total minggu',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 20.0),
                            //     child: Image.asset(
                            //       'lib/aset/print.gif',
                            //       scale: 5,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                                child: Text(
                              _tglcetak,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: FittedBox(
                                  child: Text(
                                    'Tanggal Cetak Pencairan',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 30.0),
                            //     child: Image.asset(
                            //       'lib/aset/tickbox.gif',
                            //       scale: 5,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                              child: Text(
                                _tglverif,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: FittedBox(
                                  child: Text(
                                    'Tanggal Verifikasi PPKom',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 30.0),
                            //     child: Image.asset(
                            //       'lib/aset/add.gif',
                            //       scale: 6,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                                child: Text(
                              perpanj,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: FittedBox(
                                  child: Text(
                                    'Perpanjangan perencanaan',
                                    textAlign: TextAlign.center,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 30.0),
                            //     child: Image.asset(
                            //       'lib/aset/add.gif',
                            //       scale: 6,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                                child: Text(
                              _tglbast,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: FittedBox(
                                  child: Text(
                                    'Tanggal selesai BAST',
                                    textAlign: TextAlign.center,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      //menambahkan bayangan
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            stops: [0.5, 0.8, 0.2],
                            colors: [
                              Colors.white,
                              Colors.green[100]!,
                              Colors.green[100]!
                            ],
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Center(
                            //   child: Padding(
                            //     padding: const EdgeInsets.only(top: 30.0),
                            //     child: Image.asset(
                            //       'lib/aset/add.gif',
                            //       scale: 6,
                            //     ),
                            //   ),
                            // ),
                            Spacer(),
                            Center(
                                child: Text(
                              _tglbktfsk,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                            Spacer(),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: FittedBox(
                                  child: Text(
                                    'Tanggal bukti fisik (terakhir upload)',
                                    textAlign: TextAlign.center,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
    
    //membuat customcard yang bisa kita panggil setiap kali dibutuhkan
    