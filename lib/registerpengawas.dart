// ignore_for_file: depend_on_referenced_packages, import_of_legacy_library_into_null_safe, avoid_print, prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_function_literals_in_foreach_calls, prefer_interpolation_to_compose_strings, sort_child_properties_last, no_leading_underscores_for_local_identifiers, use_build_context_synchronously, use_key_in_widget_constructors, library_private_types_in_public_api, avoid_unnecessary_containers

import 'dart:convert';
import 'dart:io';
import 'dart:core';
// import 'dart:async';
import 'package:async/async.dart';
// import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:kendali_batu/animasigeserkanan.dart';
import 'package:kendali_batu/login.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:search_choices/search_choices.dart';
import 'package:version_check/version_check.dart';
import 'model_badanusaha.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final username = TextEditingController();
  final nomor = TextEditingController();
  final nik = TextEditingController();
  final email = TextEditingController();
  final alamat = TextEditingController();

  String? perusahaan;
  String? _idbu;
  List? databp;
  List? dataNik;
  Widget? childbu;
  List<DropdownMenuItem> itembu = [];
  BadanUsaha? _badanusaha;
  PickedFile? pickedFile;
  String? filename;
  // StreamSubscription<ConnectivityResult> subscription;
  String dir = './assets/upload/ktp/';
  String? cekNik;
  String? _nik;
  String versiLokal = '';
  String versiToko = '';

  @override
  void initState() {
    super.initState();
    getBadanUsaha();
    // cekVersionLokal();
  }

  cekVersionLokal() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      versiLokal = packageInfo.version;
    });
    print(packageInfo.version);
    advancedStatusCheck();
  }

  advancedStatusCheck() async {
    // PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final versionCheck = VersionCheck(
      // iOSId: 'com.google.Vespa',
      packageVersion: versiLokal,
      packageName: 'ekenda.sda',
      showUpdateDialog: customShowUpdateDialog,
    );
    await versionCheck.checkVersion(context);

    // print(packageInfo.appName);
    // print(packageInfo.packageName);
    // print(packageInfo.version);
    // print(packageInfo.buildNumber);
  }

  void customShowUpdateDialog(BuildContext context, VersionCheck versionCheck) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: Text('Pembaruan aplikasi telah tersedia !'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                  'Segera perbarui aplikasi dari versi ${versionCheck.packageVersion}'),
              Text('ke versi ${versionCheck.storeVersion}'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Perbarui'),
            onPressed: () async {
              await versionCheck.launchStore();
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text('Tutup'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  // ignore: missing_return
  Future<String> getBadanUsaha() async {
    EasyLoading.show();
    var res = await http.get(
      // Uri.parse("http://10.0.2.2/settingandroid/getbadanusaha.php"),
      Uri.parse("http://103.211.82.84/settingandroidbatu/getbadanusaha.php"),
    );
    EasyLoading.dismiss();
    setState(() {
      databp = badanUsahaFromJson(res.body);
    });
    if (databp!.isEmpty) {
      setState(() {
        childbu = SearchChoices.single(
          displayClearIcon: false,
          underline: Padding(
            padding: EdgeInsets.all(5),
          ),
          items: itembu,
          value: _idbu,
          hint: Row(
            children: [
              Text('Pilih badan usaha'),
              Text(
                ' *',
                style: TextStyle(color: Colors.red),
              ),
            ],
          ),
          searchHint: Text("Cari badan usaha"),
          onChanged: (valuePkt) {
            setState(() {
              //minggu = ;
            });
            //getCatatan(paket);
          },
          dialogBox: true,
          isExpanded: true,
        );
      });
      EasyLoading.showError('Server Error\nMohon coba beberapa saat lagi');
    } else {
      setState(() {
        databp!.forEach((bu) {
          itembu.add(DropdownMenuItem(
            child: Padding(
              padding: const EdgeInsets.all(1.0),
              child: Text(
                '( ' + bu.npwp + ' ) ' + bu.namausaha,
              ),
            ),
            value: bu,
          ));
        });

        childbu = SearchChoices.single(
          underline: Padding(
            padding: EdgeInsets.all(5),
          ),
          displayClearIcon: false,
          items: itembu,
          value: _idbu,
          hint: Row(
            children: [
              Text('Pilih badan usaha'),
              Text(
                ' *',
                style: TextStyle(color: Colors.red),
              )
            ],
          ),
          searchHint: Text("Cari Badan Usaha"),
          onChanged: (valuePkt) {
            setState(() {
              _badanusaha = valuePkt;
              _idbu = _badanusaha!.idbu;
            });
            //getCatatan(paket);
            print(_idbu);
          },
          dialogBox: true,
          isExpanded: true,
        );
      });
    }
    return 'success';
  }

  // ignore: unused_field
  Future getImageFromGallery(context) async {
    final picker = ImagePicker();
    // ignore: deprecated_member_use
    pickedFile = await picker.getImage(
      source: ImageSource.gallery,
    );
    setState(() {
      file = File(pickedFile!.path);
    });
  }

  Future getImageFromCamera(context) async {
    final picker = ImagePicker();
    // ignore: deprecated_member_use
    pickedFile = await picker.getImage(
      preferredCameraDevice: CameraDevice.rear,
      source: ImageSource.camera,
      imageQuality: 70,
    );
    setState(() {
      file = File(pickedFile!.path);
    });
  }

  File? file;
  String? base64Image;
  Future getImage(context) async {
    // ignore: deprecated_member_use
    Widget okButton = TextButton(
      child: Text("Tutup"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Upload foto KTP"),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Row(
              // crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Image.asset(
                            'lib/aset/galleryktp.png',
                            scale: 15,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text('Gallery'),
                        )
                      ],
                    ),
                  ),
                  onTap: () async {
                    getImageFromGallery(context);
                    Navigator.of(context).pop();
                  },
                ),
                GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Image.asset(
                            'lib/aset/cameraktp.png',
                            scale: 20,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text('Camera'),
                        )
                      ],
                    ),
                  ),
                  onTap: () async {
                    getImageFromCamera(context);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
    // ignore: deprecated_member_use
  }

  final urlcekUsername =
      'http://103.211.82.84/settingandroidbatu/cek_username.php';
  // final urlcekUsername = 'http://10.0.2.2/settingandroid/cek_username.php';

  // ignore: missing_return
  Future<String> cekUsername(_nik) async {
    try {
      EasyLoading.show();
      var res = await http.post(Uri.parse(urlcekUsername), body: {'nik': _nik});

      //final resBody = json.decode(res.body);
      setState(() {
        dataNik = json.decode(res.body);
        cekNik = dataNik!.map((e) => {e['status']}).toString().replaceAll(
            RegExp(
              "[({})]",
            ),
            "");
      });
      EasyLoading.dismiss();

      if (cekNik == '0') {
        _register();
      } else {
        EasyLoading.showError(
          'NIK sudah terdaftar!',
          duration: Duration(seconds: 3),
        );
      }
    } catch (e) {
      EasyLoading.showInfo(
        '$e',
        duration: Duration(
          seconds: 3,
        ),
      );
    }
    return 'sukses';
  }

  // ignore: missing_return
  Future<String> _register() async {
    try {
      var url = Uri.parse(
          "http://103.211.82.84/settingandroidbatu/daftarpengawas.php");
      // var url = Uri.parse("http://10.0.2.2/settingandroid/daftarpengawas.php");
      // var url2 =
      //     Uri.parse("http://10.0.2.2/settingandroid/daftar_user_pass.php");
      var url2 = Uri.parse(
          "http://103.211.82.84/settingandroidbatu/daftar_user_pass.php");

      var request = http.MultipartRequest("POST", url);
      var request2 = http.MultipartRequest("POST", url2);

      // fields table data pengawas
      request.fields['idbu'] = _idbu!;
      request.fields['nama_lengkap'] = username.text;
      request.fields['nik'] = nik.text;
      request.fields['nomor'] = nomor.text;
      request.fields['email'] = email.text;
      request.fields['alamat'] = alamat.text;
      request.fields['dir'] = dir;

      // fields table xacpas
      request2.fields['nik'] = nik.text;
      // request2.fields['dir'] = dir;

      ////////////////////// kirim image definisi /////////////////////////////////////////////////////////////////////////////////

      var stream = http.ByteStream(DelegatingStream(file!.openRead()));
      var length = await file!.length();
      var multipartFile =
          http.MultipartFile("image", stream, length, filename: file!.path);
      request.files.add(multipartFile);

      EasyLoading.show(status: 'loading...');
      var response = await request.send();
      var response2 = await request2.send();

      if (response.statusCode == 200 && response2.statusCode == 200) {
        EasyLoading.showSuccess(
          'Daftar pengawas berhasil!',
          duration: Duration(seconds: 3),
        );
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => LoginAdmin(title: '')),
        );
      } else if (response.statusCode == 500 && response2.statusCode == 500) {
        EasyLoading.showError(
          'Daftar pengawas gagal!\nServer sedang sibuk\nMohon coba beberapa saat lagi',
          duration: Duration(seconds: 3),
        );
      }
      EasyLoading.dismiss();
    } catch (e) {
      debugPrint("error $e");
      // ignore: deprecated_member_use
      Widget okButton = TextButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Pendaftaran gagal"),
        content: Text("Semua kolom harus di isi dengan benar!"),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              // image: DecorationImage(
              //     image: ExactAssetImage('lib/aset/logobayang1.gif'),
              //     fit: BoxFit.cover),
            ),
            child: ListView(
              children: <Widget>[
                // Padding(
                //   padding: const EdgeInsets.only(top: 40.0),
                //   child: Image.asset(
                //     'lib/aset/logo.png',
                //     height: 190,
                //   ),
                // ),
                Center(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 10.0, top: 20, bottom: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'E',
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.w900,
                              fontStyle: FontStyle.italic,
                              fontSize: 30),
                        ),
                        Text(
                          ' - KENDA',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w900,
                              fontSize: 30),
                        ),
                      ],
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Text(
                      'HALAMAN DAFTAR PENGAWAS',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 10,
                    right: 40,
                    left: 40,
                    bottom: 30,
                  ),
                  child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black38)),
                      child: childbu),
                ),
                //Text(_idbu),
                Container(
                  padding: EdgeInsets.only(
                    right: 40,
                    left: 40,
                    bottom: 10,
                  ),
                  child: TextFormField(
                    maxLength: 16,
                    keyboardType: TextInputType.number,
                    controller: nik,
                    decoration: InputDecoration(
                      fillColor: Colors.black,
                      border: OutlineInputBorder(),
                      labelText: 'NIK',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    right: 40,
                    left: 40,
                    bottom: 30,
                  ),
                  child: TextFormField(
                    controller: username,
                    decoration: InputDecoration(
                      fillColor: Colors.black,
                      border: OutlineInputBorder(),
                      labelText: 'Nama Lengkap',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    right: 40,
                    left: 40,
                    bottom: 10,
                  ),
                  child: TextFormField(
                    maxLength: 150,
                    controller: alamat,
                    decoration: InputDecoration(
                      fillColor: Colors.black,
                      border: OutlineInputBorder(),
                      labelText: 'Alamat',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    right: 40,
                    left: 40,
                    bottom: 30,
                  ),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    //obscureText: true,
                    controller: nomor,
                    decoration: InputDecoration(
                      fillColor: Colors.black,
                      border: OutlineInputBorder(),
                      labelText: 'Nomor Telp',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    right: 40,
                    left: 40,
                    bottom: 20,
                  ),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    //obscureText: true,
                    controller: email,
                    decoration: InputDecoration(
                      fillColor: Colors.black,
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    ),
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.only(left: 42.0, bottom: 10),
                //   child: Text(
                //     "Unggah foto KTP : ",
                //     style: TextStyle(
                //       fontSize: 15,
                //       fontWeight: FontWeight.w600,
                //     ),
                //   ),
                // ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 40, right: 40, bottom: 30),
                  child: Container(
                    padding: EdgeInsets.only(
                        top: 20, bottom: 20, right: 20, left: 20),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38),
                    ),
                    child: Row(
                      children: [
                        Container(
                          // ignore: deprecated_member_use
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.grey[600], // foreground
                            ),
                            onPressed: () {
                              getImage(context);
                              // setState(() {
                              //   _nik = nik.text;
                              // });
                            },
                            child: Text('Unggah foto KTP'),
                          ),
                        ),
                        Expanded(
                          child: file != null
                              ? FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Icon(
                                    Icons.check_circle,
                                    color: Colors.green,
                                    size: 50,
                                  ),
                                )
                              : Container(
                                  child: FittedBox(
                                    fit: BoxFit.contain,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        'Belum ada foto',
                                        // maxLines: 1,
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  //height: 70,
                  child: Column(
                    children: [
                      // ignore: deprecated_member_use
                      ElevatedButton(
                        // textColor: Colors.white,
                        // color: Colors.green[400],
                        child: Text(
                          'Daftar',
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            _nik = nik.text;
                          });
                          cekUsername(_nik);
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 10, top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Sudah terdaftar ? ',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      GestureDetector(
                        child: Text(
                          ' Login',
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: 15,
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold),
                        ),
                        onTap: () {
                          Navigator.push(context,
                              geserKananHalaman(page: LoginAdmin(title: '')));
                        },
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(bottom: 30, top: 20, right: 10),
                      child: Container(
                        child: Image.asset(
                          'lib/aset/Kota_Batu.png',
                          scale: 30,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      padding: EdgeInsets.only(bottom: 30, top: 20),
                      child: Center(
                        child: Text(
                          '© Pemerintah Kota Batu\nBagian Administrasi Pembangunan',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
